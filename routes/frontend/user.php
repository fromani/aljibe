<?php

use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\CategoryController;
use App\Http\Controllers\Frontend\User\ProductController;
use App\Http\Controllers\Frontend\User\ServicePlanController;
use App\Http\Controllers\Frontend\User\DistributorController;
use App\Http\Controllers\Frontend\User\CustomerController;
use App\Http\Livewire\Address\AddressComponent;
use App\Http\Livewire\Customer\Browse as CustomerComponent;
use Tabuna\Breadcrumbs\Trail;

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the user has not confirmed their email
 */
Route::group(['as' => 'user.', 'middleware' => ['auth', 'password.expires', config('boilerplate.access.middleware.verified')]], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])
        ->middleware('is_user')
        ->name('dashboard')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('frontend.index')
                ->push(__('Dashboard'), route('frontend.user.dashboard'));
        });

    Route::get('account', [AccountController::class, 'index'])
        ->name('account')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('frontend.index')
                ->push(__('My Account'), route('frontend.user.account'));
        });

    Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    /*------------------- MAIN ROUTES ---------------------------*/
    /*-----------------------------------------------------------*/
    Route::get('categories', [CategoryController::class, 'index'])
        ->middleware(['is_user','permission:category.access'])
        ->name('categories')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('frontend.user.products')
                ->push(__('Categories'), route('frontend.user.categories'));
    });
    Route::get('products', [ProductController::class, 'index'])
        ->middleware(['is_user','permission:product.access'])
        ->name('products')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Products'), route('frontend.user.products'));
    });
    Route::get('serviceplans', [ServicePlanController::class, 'index'])
        ->middleware(['is_user','permission:serviceplan.access'])
        ->name('serviceplans')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Service Plans'), route('frontend.user.serviceplans'));
    });
    Route::get('distributors', [DistributorController::class, 'index'])
        ->middleware(['is_user','permission:distributor.access'])
        ->name('distributors')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Distributors'), route('frontend.user.distributors'));
    });
    Route::get('/customers', CustomerComponent::class)
        ->middleware(['is_user','permission:customer.access'])
        ->name('customers')
        ->breadcrumbs(function (Trail $trail) {
            $trail->push(__('Customers'), '/customers');
        });

    Route::get('/customers/{customer}/addresses', AddressComponent::class)
        ->middleware(['is_user','permission:address.access'])
        ->name('addresses')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('frontend.user.customers')
            ->push(__('Addresses'), '/customers/{customer}/addresses');
        });
});
