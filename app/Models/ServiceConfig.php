<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ServiceConfig extends Model
{
    use SoftDeletes;
    use Traits\UserUpdater;

    protected $fillable = [
        'cantidad', 'fecha_inicio', 'suspended_at', 
        'updated_by', 'service_plan_id', 'address_id',
    ];
    /*
    ** Relationships
    */
    public function periods()
    {
        return $this->hasMany(Period::class);
    }

    public function servicePlan()
    {
        return $this->belongsTo(ServicePlan::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /*
	** Accessors y Mutators
	*/
    use Traits\InicioFormatAccessor
}
