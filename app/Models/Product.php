<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'productos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'codigo', 'category_id', 'precio_actual','unidad_id', 'alicuota_iva_id', 'es_retornable', 'color',
    ];

    public function categoria()
	{
	    return $this->belongsTo(Category::class, 'category_id');
	}

	public function unit()
	{
	    return $this->belongsTo(Unit::class,'unidad_id');
	}

	public function alicuota()
	{
	    return $this->belongsTo(AlicuotaIva::class, 'alicuota_iva_id');
	}
	/*
	** Scopes 
	*/

	public function scopeOnlyRetornable($query)
    {
        return $query->where('es_retornable', true);
    }

    public function scopeOnlyNoRetornable($query)
    {
        return $query->where('es_retornable', false);
    }

    public function scopeByCategory($query, $category_id)
    {
        return $query->where('category_id', $category_id);
    }

	/*
	** Accessors y Mutators
	*/
    use Traits\Mutators;

	public function getPrecioAttribute()
	{	
		setlocale(LC_MONETARY, 'es_AR');
	    return money_format('%i', $this->precio_actual);
	}

	public function getRetornableAttribute()
	{	
	    return ($this->es_retornable ? 
                '<span class="badge badge-info">SI</span>' : 
                '<span class="badge badge-warning">NO</span>'
            );
	}

}
