<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'nombre', 'cuit', 'telefono',
    ];

    public function addresses() 
    {
    	return $this->hasMany('App\Models\Address');
    }
    /*
    ** Accessors y Mutators
    */
    use Traits\Mutators;
}
