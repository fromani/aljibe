<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configdelivery extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configdeliveries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'address_id',
                  'product_id',
                  'frecuencia',
                  'cantidad',
                  'fecha_inicio',
                  'fecha_proxima',
                  'suspended_at'
              ];
    
    /**
     * Get the address for this model.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address','address_id');
    }

    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    public function scopeOnlySuspended($query)
    {
        return $query->whereNotNull('suspended_at');
    }

    public function getEstadoAttribute()
    {   
        return ($this->deleted_at ? 
            '<span class="badge badge-danger">Eliminado</span>' : 
            ($this->suspended_at ? 
                '<span class="badge badge-warning">Suspendido</span>':
                '<span class="badge badge-success">Activo</span>')
            );
    }

}
