<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'localidad', 'codigo_postal', 'direccion', 'distributor_id', 'customer_id', 'reseller_id',
    ];
    /*
    ** Relationships
    */
    public function distributor()
    {
        return $this->belongsTo('App\Models\Distributor');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function reseller()
    {
        return $this->belongsTo('App\Models\Reseller');
    }

    public function configdeliveries()
    {
        return $this->hasMany(Configdelivery::class);
    }

    public function configservices()
    {
        return $this->hasMany(Configservice::class);
    }
    /*
	** Accessors y Mutators
	*/
    public function setLocalidadAttribute($value)
    {
        $this->attributes['localidad'] = strtoupper($value);
    }

    public function setDireccionAttribute($value)
    {
        $this->attributes['direccion'] = strtoupper($value);
    }

}
