<?php

namespace App\Models\Traits;

/**
 * Trait InicioFormatAccessor.
 */
trait InicioFormatAccessor
{

    public function getInicioAttribute()
    {
        return Carbon::parse($this->fecha_inicio)->format('d-m-Y');
    }
}