<?php

namespace App\Models\Traits;

/**
 * Trait Mutators.
 */
trait Mutators
{

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = strtoupper($value);
    }

    public function getEstadoAttribute()
    {   
        return ($this->deleted_at ? 
                '<span class="badge badge-danger">Eliminado</span>' : 
                '<span class="badge badge-success">Activo</span>'
            );
    }
}