<?php

namespace App\Models\Traits;

/**
 * Trait UserCreator.
 */
trait UserUpdater
{
    protected static function booted()
    {
        static::updated(function ($model) {
            $user = \Auth::user();
           	$model->updated_by = $user->id;
        });
    }
}