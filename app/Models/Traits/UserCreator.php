<?php

namespace App\Models\Traits;

/**
 * Trait UserCreator.
 */
trait UserCreator
{

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {
           $user = \Auth::user();
           $model->created_by = $user->id;
        });
    }
}