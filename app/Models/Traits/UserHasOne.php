<?php

namespace App\Models\Traits;

/**
 * Trait Uuid.
 */
trait UserHasOne
{

    public function user()
    {
        return $this->hasOne('App\Domains\Auth\Models\User');
    }
}