<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePlan extends Model
{
    
    use SoftDeletes;

    protected $table = 'service_plans';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'periodo', 'tipo_periodo', 'precio_actual',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_plans', 'serviceplan_id', 'product_id')
            ->withPivot('cantidad');
    }

    /*
    ** Accessors y Mutators
    */
    use Traits\Mutators;

    public function getPrecioAttribute()
    {   
        setlocale(LC_MONETARY, 'es_AR');
        return money_format('%i', $this->precio_actual);
    }


}
