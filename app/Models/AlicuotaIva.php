<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlicuotaIva extends Model
{
    protected $table = 'alicuotas_iva';
}
