<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Remito extends Model
{
    
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'remitos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'fecha_estimada',
                  'fecha_real',
                  'product_id',
                  'address_id',
                  'cantidad',
                  'precio_unitario'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * Get the product for this model.
     *
     * @return App\Models\Product
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }

    /**
     * Get the address for this model.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address','address_id');
    }

    public function getEstadoAttribute()
    {   
        return ($this->deleted_at ? 
                '<span class="badge badge-danger">Eliminado</span>' : 
                '<span class="badge badge-success">Activo</span>'
            );
    }
}
