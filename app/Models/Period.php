<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Period extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fecha_inicio', 'vencimiento', 'sevice_config_id', 
    ];
    /*
    ** Relationships
    */
    public function serviceConfig()
    {
        return $this->belongsTo(ServiceConfig::class, 'sevice_config_id');
    }	
    /*
	** Accessors y Mutators
	*/
    use Traits\InicioFormatAccessor

    public function getFinAttribute()
    {
        return Carbon::parse($this->vencimiento)->format('d-m-Y');
    }
}
