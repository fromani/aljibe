<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feeservice extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feeservices';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'fecha_desde',
                  'fecha_hasta',
                  'serviceplan_id',
                  'address_id',
                  'cantidad',
                  'precio_unitario'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * Get the serviceplan for this model.
     *
     * @return App\Models\Serviceplan
     */
    public function serviceplan()
    {
        return $this->belongsTo('App\Models\Serviceplan','serviceplan_id');
    }

    /**
     * Get the address for this model.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address','address_id');
    }

    public function getEstadoAttribute()
    {   
        return ($this->deleted_at ? 
                '<span class="badge badge-danger">Eliminado</span>' : 
                '<span class="badge badge-success">Activo</span>'
            );
    }
}
