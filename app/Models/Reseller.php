<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reseller extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nombre', 'cuit', 'telefono', 'your_user_id',
    ];

    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    /*
    ** Relationships
    */
    use Traits\UserHasOne;
    /*
    ** Accessors y Mutators
    */
    use Traits\Mutators;
}
