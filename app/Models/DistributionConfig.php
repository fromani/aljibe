<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class DistributionConfig extends Model
{
    use SoftDeletes;
    use Traits\UserUpdater;

    protected $fillable = [
        'frecuencia', 'product_id', 'cantidad', 
        'address_id', 'fecha_inicio', 'suspended_at',
        'updated_by',
    ];
	/*
    ** Relationships
    */
	public function product()
	{
	    return $this->belongsTo(Product::class);
	}

	public function servicePlan()
	{
	    return $this->belongsTo(ServicePlan::class);
	}
    /*
	** Accessors y Mutators
	*/
    use Traits\InicioFormatAccessor
}
