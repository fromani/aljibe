<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DistributionOrder extends Model
{
    use SoftDeletes;

    protected $table = 'distribution_orders';

    protected $fillable = [
        'numero', 'fecha', 'distributor_id',
    ];

    public function distributor()
    {
        return $this->belongsTo('App\Models\Distributor');
    }
}
