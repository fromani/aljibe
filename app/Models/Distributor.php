<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Distributor extends Model
{
    use SoftDeletes;
    use Traits\UserCreator;

    protected $table = 'distributors';

    protected $fillable = [
        'nombre', 'telefono', 'localidad', 'created_by', 'your_user_id',
    ];

    public function addresses()
    {
        return $this->hasMany('App\Models\Address');
    }

    public function distribution_orders()
    {
        return $this->hasMany('App\Models\DistributiOnorder');
    }
    /*
    ** Relationships
    */
    use Traits\UserHasOne;
    /*
    ** Accessors y Mutators
    */
    use Traits\Mutators;

    public function setLocalidadAttribute($value)
    {
        $this->attributes['localidad'] = strtoupper($value);
    }

}
