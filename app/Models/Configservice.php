<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configservice extends Model
{
    
    use SoftDeletes;
    use Traits\UserUpdater;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'configservices';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'cantidad',
                  'fecha_inicio',
                  'fecha_proxima',
                  'suspended_at',
                  'updated_by',
                  'service_plan_id',
                  'address_id'
              ];

    /**
     * Get the updater for this model.
     *
     * @return App\User
     */
    public function updater()
    {
        return $this->belongsTo('App\User','updated_by');
    }

    /**
     * Get the servicePlan for this model.
     *
     * @return App\Models\ServicePlan
     */
    public function servicePlan()
    {
        return $this->belongsTo('App\Models\ServicePlan','service_plan_id');
    }

    /**
     * Get the address for this model.
     *
     * @return App\Models\Address
     */
    public function address()
    {
        return $this->belongsTo('App\Models\Address','address_id');
    }

    /**
     * Get the address for this model.
     *
     * @return App\Models\Period
     */
    public function periods()
    {
        return $this->hasMany(Period::class);
    }

}
