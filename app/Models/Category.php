<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    /*use Sortable;

	public $sortable = ['categories.nombre',
                        'categories.updated_at'];*/
    use SoftDeletes;
                        
    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];
    /*
    ** Accessors y Mutators
    */
    use Traits\Mutators;

}
