<?php

namespace App\Http\Livewire\Serviceplan;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\ServicePlan;
use App\Models\Product;
use App\Models\ProductPlan;
use App\Http\Requests\Frontend\User\CreateServiceplanRequest;
use App\Http\Requests\Frontend\User\EditServiceplanRequest;
use App\Http\Requests\Frontend\User\EditProductRequest;

class Browse extends Component
{
	use WithPagination;

	public $form = [];
	//'nombre', 'periodo'
	public $nombre, $keyid, $periodo, $tipo_periodo, $producto_id, $cantidad, $dias, $precio_actual;
	public $productsplan;
	private $services;
	public $filter_active = 1;
	public $filter_removed = 0;
	public $showAddProduct;

	public function default()
    {
    	$this->nombre = "";
    	$this->periodo = 1;
        $this->tipo_periodo = "months";
    	$this->precio_actual = "";
    	$this->producto_id = Product::orderBy('nombre')->firstOr(function () {
            return (Object)["id" => ""];
        })->id; 
    	$this->cantidad = "";
    	$this->productsplan = [];
    	$this->keyid = "";
    	$this->form['title'] = "Nuevo Servicio";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    	$this->showAddProduct = 'hidde';
    	$this->dispatchBrowserEvent('notify-edit-service',['productsplan'=>$this->productsplan]);
    }

    public function mount()
    {
        $this->default();
    }

    private function filtered()
    {
    	$query = Serviceplan::with('products')->select('*');
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }else {
        	$this->filter_active = "1";
        }
    	return $query;
    }

    public function edit($id)
    {
    	$serv = ServicePlan::find($id);
    	$this->nombre = $serv->nombre;
    	$this->periodo = $serv->periodo;
        $this->tipo_periodo = $serv->tipo_periodo;
    	$this->precio_actual = $serv->precio;
    	$this->keyid = $serv->id;
    	$objProd = [];    
    	$this->productsplan = [];	
    	$prodsplan = $serv->products()->get();
    	foreach ($prodsplan as $prod) {
    		$objProd['id'] = $prod->id;
	    	$objProd['nombre'] = $prod->nombre;
	    	$objProd['pivot']['cantidad'] = $prod->pivot->cantidad;
	    	array_push(
	    		$this->productsplan, 
	    		$objProd
	    	);
    	}
    	$this->form['title'] = "Editar Servicio";
    	$this->form['action'] = "update";
    	$this->form['view'] = "edit";
    	$this->dispatchBrowserEvent('notify-edit-service',['productsplan'=>$this->productsplan]);
    }

    public function updatedDias($value)
    {
        $this->periodo = $value;
    }

    public function editing($id)
    {
    	return ($id == $this->keyid);
    }

    public function addProduct()
    {
    	if($this->cantidad > 0)
    	{
    		$prod = Product::find($this->producto_id);
	    	$objProd = [];
	    	$objProd['id'] = $prod->id;
	    	$objProd['nombre'] = $prod->nombre;
	    	$objProd['pivot']['cantidad'] = $this->cantidad;
	    	array_push(
	    		$this->productsplan, 
	    		$objProd
	    	);
    	}
    	
    }

    protected $listeners = ['quitProductItem'];

    public function quitProductItem($id)
    {
    	unset($this->productsplan[$id]);
        $this->productsplan = array_values($this->productsplan);

    }

    public function store()
    {
    	if(auth()->user()->can('serviceplan.create')) {
	    	$request = new CreateServiceplanRequest(); 
	        $validated_data = $this->validate($request->rules(), $request->messages());
	       	$serviceplan = ServicePlan::create($validated_data);
	       	$serviceplan->products()->attach(collect($this->productsplan)->pluck('id', 'product_id', 'serviceplan_id', 'pivot.cantidad'));
	       	$this->edit($serviceplan->id);
	        session()->flash('flash_success', 'Servicio agregado correctamente.');
	    }else {
	    	session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
	    }
    }

    public function update()
    {
    	if(auth()->user()->can('serviceplan.edit')) {
    		$request = new EditServiceplanRequest(); 
	        $this->validate($request->rules($this->keyid), $request->messages());
    		$serviceplan = ServicePlan::find($this->keyid);
	    	$serviceplan->update([
	    		"nombre" => $this->nombre,
	    		"periodo" => $this->periodo,
                "tipo_periodo" => $this->tipo_periodo,
	    		"precio_actual" => $this->precio_actual
	    	]);
	    	$arrayToSync = [];
	    	foreach ($this->productsplan as $item) {
	    		$arrayToSync[$item["id"]] = ["cantidad" => $item["pivot"]["cantidad"]];
	    	}
	    	count($arrayToSync)>0 ? 
	    		$serviceplan->products()->sync($arrayToSync)
	    		:$serviceplan->products()->detach();
	    	$this->default();
	    	session()->flash('flash_success', 'Servicio actualizado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    	
    }

    public function destroy($id)
    {
    	if(auth()->user()->can('serviceplan.delete')) {
	    	ServicePlan::destroy($id);
	    	session()->flash('flash_info', 'Servicio eliminado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
    	}
    }

    public function restore($id)
    {
    	if(auth()->user()->can('serviceplan.edit')) {
	    	$serviceplan = ServicePlan::onlyTrashed()->find($id);
	    	$serviceplan->restore();
	    	$this->edit($serviceplan->id);
	    	session()->flash('flash_success', 'Servicio restaurado correctamente.');
	    }else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    }

    public function render()
    {
    	$this->services = $this->filtered()->orderBy('nombre')->paginate(10);
        return view('livewire.serviceplan.browse', [
        	'services' => $this->services,
        	'products' => Product::orderBy('nombre')->get(),
        	'tipos_periodo' => [
				"days" => "Día /s",
				"weeks" => "Semana /s",
				"months" => "Mes /es"
			]
        ]);
    }
}
