<?php

namespace App\Http\Livewire;

use Livewire\Component;

abstract class AutocompleteComponent extends Component
{
    public $query;
    public $records;
    public $sizeItems;
    public $option_selected;
    public $highlightIndex;

    protected $listeners = [
    	'set-option-autocomplete' => 'setOptionAutocomplete'
    ];

    public function setOptionAutocomplete($option)
    {
        $this->option_selected = $option;
        $this->query = $this->option_selected['text'];
        $this->highlightIndex = 0;
        $this->records[$this->highlightIndex] = $this->option_selected;
    }

    public function default()
    {
        $this->query = '';
        $this->records = [];
        $this->highlightIndex = 0;
        $this->option_selected = 0;
        $this->sizeItems = -1;
    }

    public function selectOption($index = -1)
    {
    	if ($index >= 0) {
    		$this->highlightIndex = $index;
    	}    	
        $option = $this->records[$this->highlightIndex] ?? null;
        $this->query = $option['text'];
        if ($option) {
        	$this->option_selected = $option;
        	$this->records = [];
            $this->emitUp('option-autocomplete-selected', $this->option_selected);
        }
    }

    public function updatingQuery()
    {
        $this->option_selected = -1;
    }


    abstract public function updatedQuery();
    abstract public function render();
}
