<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Customer;

class Browse extends Component
{

	use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $sorted_column, $sort_direction, $data_model, $show_modal;
    public $nombre, $cuit, $telefono, $keyid;

    protected $listeners = ['modalClose' => 'closemodal'];

    protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];
    protected $rules = [
        'nombre' => 'required|string|max:120|min:4',
        'telefono' => 'nullable|string|max:25',
        'cuit' => ''
    ];
    public $search = '';

    public $filter_active = 1;
	public $filter_removed = 0;
	public $form = [];

    public function updatingSearch()
    {
        $this->resetPage();
    }

	public function default()
    {
        $this->reset(['nombre', 'cuit', 'telefono', 'keyid']);
        $this->show_modal = 0;
    	$this->form['title'] = "Nuevo Cliente";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    public function mount()
    {
        $this->sorted_column = 'nombre';
        $this->sort_direction = ['order' => 'asc', 'dir' => 'up'];
        $this->fill(request()->only('search', 'page'));
        $this->default();
    }

    private function filtered()
    {
    	$query = Customer::with('addresses')->withCount('addresses');
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }else {
        	$this->filter_active = "1";
        }
        if($this->search) {
            $query->whereLike('nombre', $this->search);
        }
    	return $query;
    }

    public function store()
    {
        if(auth()->user()->can('customer.create')) {

            $this->rules['cuit'] = 'required|numeric|unique:customers';

            $validated_data = $this->validate();
			
            $this->data_model = new Customer($validated_data);

            $this->data_model->save();

            $this->edit($this->data_model->id);

            session()->flash('flash_success', 'Cliente agregado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
        }
    }

    private function setData($id)
    {
        $this->data_model = Customer::withTrashed()->find($id);
    }

    public function edit($id)
    {
        $this->setData($id);
        $this->keyid = $this->data_model->id;
        $this->nombre = $this->data_model->nombre;
        $this->cuit = $this->data_model->cuit;
        $this->telefono = $this->data_model->telefono;
        $this->form['title'] = "Editando #{$this->keyid}";
        $this->form['action'] = "update";
        $this->form['view'] = "edit";
    }

    public function openAddresses($customer)
    {
        return redirect()->to("/customers/{$customer}/addresses");
    }

    public function editing($id)
    {
        return ($id == $this->keyid);
    }

    public function update()
    {
        if(auth()->user()->can('customer.edit')) {
            $this->rules['cuit'] = 'required|numeric|unique:customers,cuit,'.$this->keyid.',id';
            $this->validate();
            $this->data_model->nombre = $this->nombre;
            $this->data_model->cuit = $this->cuit;
            $this->data_model->telefono = $this->telefono;
            $this->data_model->save();
            $this->default();
            session()->flash('flash_success', 'Cliente actualizado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
        
    }

    public function destroy($id)
    {
        if(auth()->user()->can('customer.delete')) {
            Customer::destroy($id);
            $this->default();
            session()->flash('flash_info', 'Cliente eliminado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
        }
    }

    public function restore($id)
    {
        if(auth()->user()->can('customer.edit')) {
        	$this->edit($id);

            $this->data_model->restore();
            
            session()->flash('flash_success', 'Cliente restaurada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
    }

    public function sortBy($col_name)
    {    
        $directions = collect([['order' => 'asc', 'dir' => 'up'], ['order' => 'desc', 'dir' => 'down']]);

        $new_direction = $directions->where('order','!=', $this->sort_direction['order'])->first();

        $this->sort_direction['dir'] = $new_direction['dir'];
        $this->sort_direction['order'] = $new_direction['order'];

        $this->sorted_column = $col_name;
    }

    public function render()
    {
    	$space = 'livewire.customer';
    	$list_customers = $this->filtered()->orderBy($this->sorted_column, $this->sort_direction['order'])->paginate(12);
        return view("{$space}.browse", [
        	'includes' => "{$space}.includes",
        	'list_customers' => $list_customers
        ])
            ->extends('frontend.layouts.app')
            ->section('content');
    }
}
