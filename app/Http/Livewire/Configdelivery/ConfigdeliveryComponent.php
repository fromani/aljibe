<?php

namespace App\Http\Livewire\Configdelivery;

use Livewire\Component;
use App\Models\Address;
use App\Models\Configdelivery;
use App\Models\Product;

class ConfigdeliveryComponent extends Component
{
    public $data_model, 
		   $frecuencia, 
		   $cantidad, 
		   $fecha_inicio,
		   $product_id,
           $suspended,
           $suspended_at,
		   $keyid,
		   $address;
	public $sorted_column, $sort_direction; 
	public $form = [];
	public $filter_active = 1;
	public $filter_removed = 0;
	public $filter_suspended = 0;
    public $search = '';
    protected $queryString = [
        'search' => ['except' => ''],
    ];
    protected $rules = [
        'frecuencia'   => 'required|integer|min:0',
        'cantidad'     => 'required|integer|min:0',
        'fecha_inicio' => 'required|date',
        'product_id'  => 'required|integer|min:0',
        //'suspended_at' => 'nullable|date'
    ];

    public function mount(Address $address)
    {
    	$this->address = $address;
    	$this->default();
    }

    public function default()
    {
        $this->reset(['frecuencia', 'cantidad', 'fecha_inicio', 'product_id', 'keyid']);
        $this->suspended = 0;
        $this->suspended_at = now();
    	$this->form['title'] = "Nueva Configuración De Reparto";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    private function filtered()
    {
      	$query = Configdelivery::with(['product', 'address'])->where('address_id', $this->address->id);
      
      	if($this->filter_active && $this->filter_removed){
          $query->withTrashed();
        }elseif ($this->filter_removed) {
          $query->onlyTrashed();
        }else {
          $this->filter_active = "1";
        }
        if($this->filter_suspended){
          $query->onlySuspended();
        }

      	return $query;
    }

    public function editing($id)
    {
        return ($id == $this->keyid);
    }

    public function store()
    {
        if(auth()->user()->can('configdelivery.create')) {

            $validated_data = $this->validate();

            $this->data_model = new Configdelivery($validated_data);

            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->data_model->fecha_inicio);
            $date->addDays($this->data_model->frecuencia-1);

            $this->data_model->fecha_proxima = $date;

            $this->address->configdeliveries()->save($this->data_model);

            $this->edit($this->data_model->id);

            session()->flash('flash_success', 'Configuración agregada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
        }
    }

    private function setData($id)
    {
        $this->data_model = Configdelivery::withTrashed()->find($id);
    }

    public function edit($id)
    {
        $this->setData($id);
        $this->keyid = $this->data_model->id;
        $this->product_id = $this->data_model->product_id;
        $this->cantidad = $this->data_model->cantidad;
        $this->frecuencia = $this->data_model->frecuencia;
        $this->fecha_inicio = $this->data_model->fecha_inicio;
        $this->suspended = $this->data_model->suspended_at ? 1 : 0;
        $this->form['title'] = "Editando #{$this->keyid}";
        $this->form['action'] = "update";
        $this->form['view'] = "edit";
    }

    public function update()
    {
        if(auth()->user()->can('configdelivery.edit')) {

            $this->validate();

            $this->data_model->suspended_at = $this->suspended ? Date('Y-m-d') : null;
            $this->data_model->product_id = $this->product_id;
            $this->data_model->cantidad = $this->cantidad;
            $this->data_model->frecuencia = $this->frecuencia;
            $this->data_model->fecha_inicio = $this->fecha_inicio;

            $this->address->configdeliveries()->save($this->data_model);

            $this->default();

            session()->flash('flash_success', 'Configuración actualizada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
        
    }

    public function destroy($id)
    {
        if(auth()->user()->can('configdelivery.delete')) {
            Configdelivery::destroy($id);
            $this->default();
            session()->flash('flash_info', 'Configuración eliminada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
        }
    }

    public function restore($id)
    {
        if(auth()->user()->can('configdelivery.edit')) {
            $this->edit($id);

            $this->data_model->restore();
            
            session()->flash('flash_success', 'Configuración restaurada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
    }

    public function render()
    {
        $list_configs = $this->filtered()->get();
        $products = Product::orderBy('nombre')->get();
    	$space = 'livewire.configdelivery';
        return view("{$space}.configdelivery-component",[
        	'includes' => "{$space}.includes",
            'products' => $products,
            'list_configs' => $list_configs
        ]);
    }
}
