<?php

namespace App\Http\Livewire\Product;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Product;
use App\Models\Category;
use App\Models\Unit;
use App\Models\AlicuotaIva;
use App\Http\Requests\Frontend\User\CreateProductRequest;
use App\Http\Requests\Frontend\User\EditProductRequest;

class Browse extends Component
{
	use WithPagination;

	public $form = [];
	public $nombre, $keyid, $codigo, $category_id, $precio_actual, $unidad_id, $alicuota_iva_id, $es_retornable, $color;
	public $filter_active = 1;
	public $filter_removed = 0;
	public $filter_retornable = 1;
	public $filter_noretornable = 1;
	public $filter_category = "";
	private $products;

	public function mount()
    {
        $this->default();
    }

    private function generateEAN($number)
	{
		$code = '500' . str_pad($number, 9, '0');
		$weightflag = true;
		$sum = 0;
		for ($i = strlen($code) - 1; $i >= 0; $i--) {
			$sum += (int)$code[$i] * ($weightflag?3:1);
			$weightflag = !$weightflag;
		}
		$code .= (10 - ($sum % 10)) % 10;
		return $code;
	}

	public function refreshColor()
	{
		$this->color = "#".dechex(rand(0x000000, 0xFFFFFF));
	}

    public function default()
    {
    	$this->nombre = "";
    	$this->codigo = $this->generateEAN(rand (10 , 999999999));
    	$this->category_id = Category::orderBy('nombre')->first()->id;
    	$this->precio_actual = "";
    	$this->unidad_id = "1";
    	$this->alicuota_iva_id = "7";
    	$this->es_retornable = 0;
    	$this->color = "#".dechex(rand(0x000000, 0xFFFFFF));
    	$this->keyid = "";
    	$this->form['title'] = "Nuevo Producto";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    private function filtered()
    {
    	$query = Product::select('*');
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }else {
        	$this->filter_active = "1";
        }
        if($this->filter_retornable && !$this->filter_noretornable){
        	$query->onlyRetornable();
        }elseif (!$this->filter_retornable && $this->filter_noretornable) {
        	$query->onlyNoRetornable();
        }else {
        	$this->filter_retornable = 1;
			$this->filter_noretornable = 1;
        }
        if($this->filter_category) {
        	$query->byCategory($this->filter_category);
        }
    	return $query;
    }

    public function store()
    {
    	if(auth()->user()->can('product.edit')) {
	    	$request = new CreateProductRequest(); 
	        $validated_data = $this->validate($request->rules(), $request->messages());
	       	$product = Product::create($validated_data);
	       	$this->edit($product->id);
	        session()->flash('flash_success', 'Producto agregado correctamente.');
	    }else {
	    	session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
	    }
    }

    public function edit($id)
    {
    	$prod = Product::find($id);
    	$this->nombre = $prod->nombre;
    	$this->codigo = $prod->codigo;
    	$this->category_id = $prod->category_id;
    	$this->precio_actual = $prod->precio_actual;
    	$this->unidad_id = $prod->unidad_id;
    	$this->alicuota_iva_id = $prod->alicuota_iva_id;
    	$this->es_retornable = $prod->es_retornable;
    	$this->color = $prod->color;
    	$this->keyid = $prod->id;
    	$this->form['title'] = "Editar Producto";
    	$this->form['action'] = "update";
    	$this->form['view'] = "edit";
    }

    public function editing($id)
    {
    	return ($id == $this->keyid);
    }

    public function update()
    {
    	if(auth()->user()->can('product.edit')) {
    		$request = new EditProductRequest(); 
	        $this->validate($request->rules($this->keyid), $request->messages());
    		$product = Product::find($this->keyid);

	    	$product->update([
	    		"nombre" => $this->nombre,
	    		"codigo" => $this->codigo,
	    		"category_id" => $this->category_id,
	    		"precio_actual" => $this->precio_actual,
	    		"unidad_id" => $this->unidad_id,
	    		"alicuota_iva_id" => $this->alicuota_iva_id,
	    		"es_retornable" => $this->es_retornable,
	    		"color" => $this->color,
	    	]);
	    	$this->default();
	    	session()->flash('flash_success', 'Producto actualizado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    	
    }

    public function destroy($id)
    {
    	if(auth()->user()->can('product.delete')) {
	    	Product::destroy($id);
	    	session()->flash('flash_info', 'Producto eliminado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
    	}
    }

    public function restore($id)
    {
        if(auth()->user()->can('product.edit')) {
            $product = Product::onlyTrashed()->find($id);
            $product->restore();
            $this->edit($product->id);
            session()->flash('flash_success', 'Producto restaurado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
    }

    public function render()
    {
        $this->products = $this->filtered()->orderBy('nombre')->paginate(10);
        return view('livewire.product.browse', [
            'products' => $this->products,
            'categories' => Category::orderBy('nombre')->get(),
            'units' => Unit::orderBy('nombre')->get(),
            'alicuotas' => AlicuotaIva::orderBy('descripcion')->get(),
        ]);
    }
}
