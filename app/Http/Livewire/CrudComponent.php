<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

abstract class CrudComponent extends Component
{
	public $data_model, $keyid;
	public $form = [];
	public $filter_active = 1;
	public $filter_removed = 0;
    public $search = '';
    public $sorted_column;
    public $sort_direction;
    public $current_user;

    use WithPagination;
    
    protected $paginationTheme = 'bootstrap';

	protected $queryString = [
        'search' => ['except' => ''],
        'page' => ['except' => 1],
    ];

    public function default()
    {
      	$this->current_user = \Auth::user();
      	$this->sort_direction = ['order' => 'asc', 'dir' => 'up'];
      	$this->fill(request()->only('search', 'page'));
      	$this->form['action'] = "store";
      	$this->form['view'] = "create";
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function editing($id)
    {
        return ($id == $this->keyid);
    }

    public function sortBy($col_name)
    {    
        $directions = collect([['order' => 'asc', 'dir' => 'up'], ['order' => 'desc', 'dir' => 'down']]);

        $new_direction = $directions->where('order','!=', $this->sort_direction['order'])->first();

        $this->sort_direction['dir'] = $new_direction['dir'];
        $this->sort_direction['order'] = $new_direction['order'];

        $this->sorted_column = $col_name;
    }

    abstract public function dataReset();
    abstract protected function filtered();
    abstract public function store();
    abstract public function edit($id);
    abstract public function update();
    abstract public function destroy($id);
    abstract public function restore($id);
}