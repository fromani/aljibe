<?php

namespace App\Http\Livewire\Configservice;

use Livewire\Component;
use App\Models\ServicePlan;
use App\Models\Configservice;
use App\Models\Address;

class ConfigserviceComponent extends Component
{
    public $data_model, 
           $cantidad, 
           $fecha_inicio,
           $service_plan_id,
           $suspended,
           $suspended_at,
           $keyid,
           $address;
    public $sorted_column, $sort_direction; 
    public $form = [];
    public $filter_active = 1;
    public $filter_removed = 0;
    public $filter_suspended = 0;
    public $search = '';
    protected $queryString = [
        'search' => ['except' => ''],
    ];
    protected $rules = [
        'cantidad'     => 'required|integer|min:0',
        'fecha_inicio' => 'required|date',
        'service_plan_id'  => 'required|integer|min:0',
        //'suspended_at' => 'nullable|date'
    ];

    public function mount(Address $address)
    {
        $this->address = $address;
        $this->default();
    }

    public function default()
    {
        $this->reset(['cantidad', 'fecha_inicio', 'service_plan_id', 'keyid']);
        $this->suspended = 0;
        $this->suspended_at = now();
        $this->form['title'] = "Nueva Configuración De Servicio";
        $this->form['action'] = "store";
        $this->form['view'] = "create";
    }

    private function filtered()
    {
        $query = Configservice::with(['servicePlan', 'address'])->where('address_id', $this->address->id);
      
        if($this->filter_active && $this->filter_removed){
          $query->withTrashed();
        }elseif ($this->filter_removed) {
          $query->onlyTrashed();
        }else {
          $this->filter_active = "1";
        }
        if($this->filter_suspended){
          $query->onlySuspended();
        }

        return $query;
    }

    public function editing($id)
    {
        return ($id == $this->keyid);
    }

    public function store()
    {
        if(auth()->user()->can('configservice.create')) {

            $validated_data = $this->validate();

            $this->data_model = new Configservice($validated_data);

            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $this->data_model->fecha_inicio);
            $tipo_periodo = $this->data_model->servicePlan->tipo_periodo;
            $periodo = $this->data_model->servicePlan->periodo;
            $date->add($periodo, $tipo_periodo);

            $this->data_model->fecha_proxima = $date;

            $this->address->configservices()->save($this->data_model);

            $this->edit($this->data_model->id);

            session()->flash('flash_success', 'Configuración agregada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
        }
    }

    private function setData($id)
    {
        $this->data_model = Configservice::withTrashed()->find($id);
    }

    public function edit($id)
    {
        $this->setData($id);
        $this->keyid = $this->data_model->id;
        $this->service_plan_id = $this->data_model->service_plan_id;
        $this->cantidad = $this->data_model->cantidad;
        $this->frecuencia = $this->data_model->frecuencia;
        $this->fecha_inicio = $this->data_model->fecha_inicio;
        $this->suspended = $this->data_model->suspended_at ? 1 : 0;
        $this->form['title'] = "Editando #{$this->keyid}";
        $this->form['action'] = "update";
        $this->form['view'] = "edit";
    }

    public function update()
    {
        if(auth()->user()->can('configservice.edit')) {

            $this->validate();

            $this->data_model->suspended_at = $this->suspended ? Date('Y-m-d') : null;
            $this->data_model->service_plan_id = $this->service_plan_id;
            $this->data_model->cantidad = $this->cantidad;
            $this->data_model->frecuencia = $this->frecuencia;
            $this->data_model->fecha_inicio = $this->fecha_inicio;

            $this->address->configservices()->save($this->data_model);

            $this->default();

            session()->flash('flash_success', 'Configuración actualizada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
        
    }

    public function destroy($id)
    {
        if(auth()->user()->can('configservice.delete')) {
            Configservice::destroy($id);
            $this->default();
            session()->flash('flash_info', 'Configuración eliminada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
        }
    }

    public function restore($id)
    {
        if(auth()->user()->can('configservice.edit')) {
            $this->edit($id);

            $this->data_model->restore();
            
            session()->flash('flash_success', 'Configuración restaurada correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
    }

    public function render()
    {
    	$list_configs = $this->filtered()->get();
        $services = ServicePlan::orderBy('nombre')->get();
    	$space = 'livewire.configservice';
        return view("{$space}.configservice-component",[
        	'includes' => "{$space}.includes",
            'services' => $services,
            'list_configs' => $list_configs
        ]);
    }
}
