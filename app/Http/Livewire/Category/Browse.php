<?php

namespace App\Http\Livewire\Category;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Category;
use App\Http\Requests\Frontend\User\CreateCategoryRequest;
use App\Http\Requests\Frontend\User\EditCategoryRequest;

class Browse extends Component
{
	use WithPagination;

	public $form = [];
	public $nombre, $keyid;
	public $filter_active = "1";
	public $filter_removed = "";
	private $categories;

	public function mount()
    {
        $this->default();
    }

    private function filtered()
    {
    	$query = Category::select('*');
    	$this->filter_active = "1";
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }
    	return $query;
    }

    public function render()
    {
    	
        $this->categories = $this->filtered()->orderBy('nombre')->paginate(10);
        return view('livewire.category.browse', [
            'categories' => $this->categories,
        ]);
    }

    public function store()
    {
    	if(auth()->user()->can('category.edit')) {
	    	$request = new SaveCategoryRequest(); 
	        $validated_data = $this->validate($request->rules(), $request->messages());
	       	$category = Category::create($validated_data);
	       	$this->edit($category->id);
	        session()->flash('flash_success', 'Categoría agregada correctamente.');
	    }else {
	    	session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
	    }
    }

    public function restore($id)
    {
    	if(auth()->user()->can('category.edit')) {
	    	$category = Category::onlyTrashed()->find($id);
	    	$category->restore();
	    	$this->edit($category->id);
	    	session()->flash('flash_success', 'Categoría restaurada correctamente.');
	    }else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    }

    public function edit($id)
    {
    	$categ = Category::find($id);
    	$this->nombre = $categ->nombre;
    	$this->keyid = $categ->id;
    	$this->form['title'] = "Editar Categoría";
    	$this->form['action'] = "update";
    	$this->form['view'] = "edit";
    }

    public function editing($id)
    {
    	return ($id == $this->keyid);
    }

    public function update()
    {
    	if(auth()->user()->can('category.edit')) {
    		$request = new EditCategoryRequest(); 
	        $this->validate($request->rules($this->keyid), $request->messages());
    		$category = Category::find($this->keyid);
	    	$category->update([
	    		"nombre" => $this->nombre
	    	]);
	    	$this->default();
	    	session()->flash('flash_success', 'Categoría actualizada correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    	
    }

    public function default()
    {
    	$this->nombre = "";
    	$this->keyid = "";
    	$this->form['title'] = "Nueva Categoría";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    public function destroy($id)
    {
    	if(auth()->user()->can('category.delete')) {
	    	Category::destroy($id);
	    	session()->flash('flash_success', 'Categoría eliminada correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
    	}
    }
    
}
