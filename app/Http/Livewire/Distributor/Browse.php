<?php

namespace App\Http\Livewire\Distributor;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Distributor;
use App\Http\Requests\Frontend\User\DistributorRequest;

class Browse extends Component
{

	use WithPagination;

	public $form = [];
	//'nombre', 'telefono'
	public $nombre, $keyid, $telefono, $localidad;
	private $distributors;
	public $filter_active = 1;
	public $filter_removed = 0;
    public $filter_city = '';

	public function default()
    {
    	$this->nombre = "";
    	$this->telefono = "";
        $this->localidad = "LARROQUE";
    	$this->keyid = "";
    	$this->form['title'] = "Nuevo Repartidor";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    public function mount()
    {
        $this->default();
    }

    private function filtered()
    {
    	$query = Distributor::with('addresses')->select('*');
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }else {
        	$this->filter_active = "1";
        }
        if($this->filter_city) {
            $query->whereLike('localidad', $this->filter_city);
        }
    	return $query;
    }

    public function store()
    {
    	if(auth()->user()->can('distributor.create')) {
	    	$request = new DistributorRequest(); 
	        $validated_data = $this->validate($request->rules(), $request->messages());
	       	$distributor = Distributor::create($validated_data);
	       	$this->edit($distributor->id);
	        session()->flash('flash_success', 'Repartidor agregado correctamente.');
	    }else {
	    	session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
	    }
    }

    public function edit($id)
    {
    	$dist = Distributor::find($id);
    	$this->nombre = $dist->nombre;
    	$this->telefono = $dist->telefono;
        $this->localidad = $dist->localidad;
    	$this->keyid = $dist->id;	

    	$this->form['title'] = "Editar Repartidor";
    	$this->form['action'] = "update";
    	$this->form['view'] = "edit";
    }

    public function editing($id)
    {
    	return ($id == $this->keyid);
    }

    public function update()
    {
    	if(auth()->user()->can('distributor.edit')) {
    		$request = new DistributorRequest(); 
	        $this->validate($request->rules($this->keyid), $request->messages());
    		$distributor = Distributor::find($this->keyid);
	    	$distributor->update([
	    		"nombre" => $this->nombre,
	    		"telefono" => $this->telefono,
                "localidad" => $this->localidad
	    	]);
	    	$this->default();
	    	session()->flash('flash_success', 'Repartidor actualizado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    	
    }

    public function destroy($id)
    {
    	if(auth()->user()->can('distributor.delete')) {
	    	Distributor::destroy($id);
	    	$this->default();
	    	session()->flash('flash_info', 'Repartidor eliminado correctamente.');
    	}else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
    	}
    }

    public function restore($id)
    {
    	if(auth()->user()->can('distributor.edit')) {
	    	$distributor = Distributor::onlyTrashed()->find($id);
	    	$distributor->restore();
	    	$this->edit($distributor->id);
	    	session()->flash('flash_success', 'Repartidor restaurado correctamente.');
	    }else {
    		session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
    	}
    }

    public function render()
    {
    	$this->distributors = $this->filtered()->orderBy('nombre')->paginate(10);
        return view('livewire.distributor.browse', [
        	'distributors' => $this->distributors
        ]);
    }
}
