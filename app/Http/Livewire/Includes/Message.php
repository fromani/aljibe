<?php

namespace App\Http\Livewire\Includes;

use Livewire\Component;

class Message extends Component
{
    public function render()
    {
        return view('livewire.includes.message');
    }
}
