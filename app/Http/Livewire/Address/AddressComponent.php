<?php

namespace App\Http\Livewire\Address;

use Livewire\Component;
use App\Models\Address;
use App\Models\Customer;
use App\Models\Reseller;
use App\Models\Distributor;
use Livewire\WithPagination;

class AddressComponent extends Component
{

	public $data_model, $show_modal;
    public $localidad, $codigo_postal, $direccion, $distributor_id, $customer, $reseller_id, $keyid;

    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['modalClose' => 'closemodal'];

    protected $rules = [
        'localidad' => 'required|string|max:120|min:4',
        'codigo_postal' => 'nullable|integer',
        'direccion' => 'required|string|max:120|min:4',
        'distributor_id' => 'nullable|integer',
        'reseller_id' => 'nullable|integer',
        //'customer' => 'required|integer'
    ];

	public $filter_active = 1;
	public $filter_removed = 0;
	public $form = [];

	public function default()
    {
        $this->reset(['codigo_postal', 'direccion', 'distributor_id', 'reseller_id', 'keyid']);
        $this->show_modal = 0;
        $this->localidad = 'LARROQUE';
    	$this->form['title'] = "Nuevo Domicilio";
    	$this->form['action'] = "store";
    	$this->form['view'] = "create";
    }

    public function mount(Customer $customer)
    {
        $this->customer = $customer;
        $this->default();
    }

	private function filtered()
    {
    	$query = Address::where('customer_id', $this->customer->id);
    	if($this->filter_active && $this->filter_removed){
        	$query->withTrashed();
        }elseif ($this->filter_removed) {
        	$query->onlyTrashed();
        }else {
        	$this->filter_active = "1";
        }
    	return $query;
    }

    public function store()
    {
        if(auth()->user()->can('customer.create')) {

            $validated_data = $this->validate();

            $this->data_model = new Address($validated_data);

            $this->customer->addresses()->save($this->data_model);

            $this->edit($this->data_model->id);

            session()->flash('flash_success', 'Domicilio agregado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para agregar.');
        }
    }

    private function setData($id)
    {
        $this->data_model = Address::withTrashed()->find($id);
    }

    public function edit($id)
    {
        $this->setData($id);
        $this->keyid = $this->data_model->id;
        $this->localidad = $this->data_model->localidad;
        $this->codigo_postal = $this->data_model->codigo_postal;
        $this->direccion = $this->data_model->direccion;
        $this->distributor_id = $this->data_model->distributor_id;
        $this->form['title'] = "Editando #{$this->keyid}";
        $this->form['action'] = "update";
        $this->form['view'] = "edit";
    }

    public function editing($id)
    {
        return ($id == $this->keyid);
    }

    public function update()
    {
        if(auth()->user()->can('customer.edit')) {
            $this->validate();
            $this->data_model->localidad = $this->localidad;
            $this->data_model->codigo_postal = $this->codigo_postal;
            $this->data_model->direccion = $this->direccion;
            $this->data_model->distributor_id = $this->distributor_id;
            $this->data_model->reseller_id = $this->reseller_id;
            $this->customer->addresses()->save($this->data_model);
            $this->default();
            session()->flash('flash_success', 'Domicilio actualizado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
        
    }

    public function modalconfig($id)
    {
        $this->setData($id);
        $this->show_modal = 1;
        $this->dispatchBrowserEvent('open-modal', ['open_modal' => 1]);
    }

    public function closemodal()
    {
        $this->show_modal = 0;
    }

    public function destroy($id)
    {
        if(auth()->user()->can('address.delete')) {
            Address::destroy($id);
            $this->default();
            session()->flash('flash_info', 'Domicilio eliminado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para eliminar.');
        }
    }

    public function restore($id)
    {
        if(auth()->user()->can('address.edit')) {
            $this->edit($id);

            $this->data_model->restore();
            
            session()->flash('flash_success', 'Domicilio restaurado correctamente.');
        }else {
            session()->flash('flash_warning', 'Usted no tiene permisos para editar.');
        }
    }

    public function render()
    {
    	$resellers = Reseller::orderBy('nombre')->get();
        $distributors = Distributor::orderBy('nombre')->get();
        $space = 'livewire.address';
    	$list_address = $this->filtered()->paginate(10);
        return view("{$space}.address-component", [
        	'includes' => "{$space}.includes",
        	'list_address' => $list_address,
            'resellers' => $resellers,
            'distributors' => $distributors
        ])
        ->extends('frontend.layouts.app')
        ->section('content');;
    }
}
