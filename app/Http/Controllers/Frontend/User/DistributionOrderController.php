<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\DistributionOrder;
use Illuminate\Http\Request;

class DistributionOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DistributionOrder  $distributionOrder
     * @return \Illuminate\Http\Response
     */
    public function show(DistributionOrder $distributionOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DistributionOrder  $distributionOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(DistributionOrder $distributionOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DistributionOrder  $distributionOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DistributionOrder $distributionOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DistributionOrder  $distributionOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(DistributionOrder $distributionOrder)
    {
        //
    }
}
