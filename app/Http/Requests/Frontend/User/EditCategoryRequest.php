<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Foundation\Http\FormRequest;

class EditCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($id)
    {
        return [
            'id' => 'required',
            'nombre' => 'required|unique:categories,nombre,'.$id.'|max:120',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El campo :attribute es requerido.',
            'nombre.max'      => 'El valor :input supera el tamaño máximo (:max) permitido.',
            'nombre.unique'   => ':input ya existe.',
        ];
    }
}
