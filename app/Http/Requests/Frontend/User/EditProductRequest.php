<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Foundation\Http\FormRequest;

class EditProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($id)
    {
        return [
            'id'              => 'required',
            'nombre'          => 'required|max:120',
            'codigo'          => 'required|unique:productos,codigo,'.$id.'|max:30',
            'category_id'     => 'required|integer',
            'precio_actual'   => 'required|numeric|max:9999999999',
            'unidad_id'       => 'required|integer',
            'alicuota_iva_id' => 'required|integer',
            'es_retornable'   => 'required',
            'color'           => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'El campo :attribute es requerido.',
            'max'           => 'El valor :input puede contener un máximo de (:max) caracteres.',
            'codigo.unique' => 'El :attribute :input ya existe.',
            'numeric'       => 'El campo :attribute debe ser numérico.',
        ];
    }
}
