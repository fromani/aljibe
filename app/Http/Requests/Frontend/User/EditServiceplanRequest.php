<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Foundation\Http\FormRequest;

class EditServiceplanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('serviceplan.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($id)
    {
        return [
            'id'      => 'nullable',
            'nombre'  => 'required|max:120',
            'tipo_periodo' => 'required|in:days,weeks,months',
            'periodo' => 'required_without:tipo_periodo|integer',
            'precio_actual' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'El campo :attribute es requerido.',
            'required_without' => 'El campo :attribute es requerido.',
            'max'           => 'El valor :input puede contener un máximo de (:max) caracteres.',
            'integer'       => 'El campo :attribute debe ser numérico.',
            'in'               => 'El campo :attribute no es válido.',
        ];
    }
}
