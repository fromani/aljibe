<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Blade;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            $this->where(function (Builder $query) use ($attributes, $searchTerm) {
                foreach (\Arr::wrap($attributes) as $attribute) {
                    $query->orWhere($attribute, 'iLIKE', "%{$searchTerm}%");
                }
            });
            return $this;
        });

        Blade::directive('money', function ($value) {
            return "<?php echo '$ ' . number_format($value, 2, '.', ',') ?>";
        });

        Blade::directive('dmy', function ($value) {
            return "<?php echo \Carbon\Carbon::parse($value)->format('d/m/Y') ?>";
        });
	
	Blade::directive('percent', function ($value) {
            return "<?php echo number_format($value, 2, '.', ',').' %' ?>";
        });
    }
}
