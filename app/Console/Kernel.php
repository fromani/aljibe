<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

/**
 * Class Kernel.
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateFechaProximaDelivery::class,
        Commands\UpdateFechaProximaService::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
        $schedule->command('delivery:fechaproxima')
            ->dailyAt('01:00')
            ->emailOutputOnFailure('federico.romani@swai.com.ar')
            ->environments(['staging', 'production']); // only production

        $schedule->command('delivery:fechaproxima')
            ->cron('* * * * *') 
            ->environments(['staging', 'local']); // only local

        $schedule->command('service:fechaproxima')
            ->dailyAt('01:05')
            ->emailOutputOnFailure('federico.romani@swai.com.ar')
            ->environments(['staging', 'production']); // only production

        $schedule->command('delivery:fechaproxima')
            ->cron('* * * * *') 
            ->environments(['staging', 'local']); // only local
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
