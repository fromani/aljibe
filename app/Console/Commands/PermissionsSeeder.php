<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PermissionsSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:PermissionsSeeder {name} {entity}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un seeder para permisos';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $name = $this->argument('name');
      $entity = $this->argument('entity');
      if ($entity === '' || is_null($entity) || empty($entity)) {
        return $this->error('Falta la entidad..!');
      }

      $contents = '<?php
                  use App\Domains\Auth\Models\Permission;
                  use Illuminate\Database\Seeder;
                  use App\Domains\Auth\Models\User;
            
                  class ' . $name . ' extends Seeder
                  {
                    use DisableForeignKeys;
                    public function run()
                    {
                      $this->disableForeignKeys();
                      $'. $entity .' = Permission::create([
                        "type" => User::TYPE_USER,
                        "name" => "'. $entity .'.access",
                        "description" => "Acceso a '. $entity .'"
                      ]);

                      $'. $entity .'->children()->saveMany([
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "'. $entity .'.show",
                            "description" => "Consultar '. $entity .'",
                            "sort" => 1,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "'. $entity .'.create",
                            "description" => "Agregar '. $entity .'",
                            "sort" => 2,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "'. $entity .'.edit",
                            "description" => "Modificar datos de '. $entity .'",
                            "sort" => 3,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "'. $entity .'.delete",
                            "description" => "Eliminar '. $entity .'",
                            "sort" => 4,
                        ]),
                      ]);
                    }
                  }';
      $file = $this->laravel->databasePath().'/seeds/'.$name.'.php';
      if (!file_put_contents($file, $contents))
          return $this->error('Error al crear el seeder!');
      $this->info("$name generated!");
    }
}
