<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateFechaProximaService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:fechaproxima';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza la fecha proximo vencimiento de servicio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $records = \App\Models\Configservice::where('fecha_proxima','<',\Carbon\Carbon::now())->get();
        foreach ($records as $data_model) {

            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $data_model->fecha_proxima);

            $tipo_periodo = $data_model->servicePlan->tipo_periodo;

            $periodo = $data_model->servicePlan->periodo;

            $date->add($periodo, $tipo_periodo);

            $data_model->fecha_proxima = $date;

            $data_model->save();
        }
        $this->info('Ejecucion de service:fechaproxima correcta.'); 
    }
}
