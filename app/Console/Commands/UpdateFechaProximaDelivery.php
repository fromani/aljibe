<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateFechaProximaDelivery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delivery:fechaproxima';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza la fecha proxima de reparto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $records = \App\Models\Configdelivery::where('fecha_proxima','<',\Carbon\Carbon::now())->get();
        foreach ($records as $data_model) {

            $date = \Carbon\Carbon::createFromFormat('Y-m-d', $data_model->fecha_proxima);
        
            $date->addDays($data_model->frecuencia-1);

            $data_model->fecha_proxima = $date;

            $data_model->save();
        }        
        $this->info('Ejecucion de delivery:fechaproxima correcta.');
    }
}
