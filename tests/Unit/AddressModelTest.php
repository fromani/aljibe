<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use App\Models\Address;
use App\Models\Distributor;
use App\Models\Customer;
use App\Models\Reseller;
//php artisan make:test AddressModelTest --unit
class AddressModelTest extends TestCase
{
	use RefreshDatabase, WithFaker;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    /*public function testColumns()
    {
        $this->assertTrue( 
      		Schema::hasColumns('addresses', [
            	'localidad', 'codigo_postal', 'direccion', 'distributor_id', 'customer_id', 'reseller_id'
        ]), 1);
    }*/
    /** @test */
    public function belongs_to_distributor()
    {
        $distributor = factory(Distributor::class)->create();
        
        $address = factory(Address::class)->create([
        	'distributor_id' => $distributor->id
        ]); 
        $this->assertInstanceOf(Distributor::class, $address->distributor);
    }
    /** @test *//*
    public function belongs_to_customerTest()
    {
        $customer = factory(Customer::class)->create();
        $address = factory(Address::class)->create([
        	'customer_id' => $customer->id
        ]); 
        $this->assertInstanceOf(Customer::class, $address->customer);
    }*/
	/** @test *//*
    public function belongs_to_reseller()
    {
        $reseller = factory(Reseller::class)->create();
        $address = factory(Address::class)->create([
        	'reseller_id' => $reseller->id
        ]); 
        $this->assertInstanceOf(Reseller::class, $address->reseller);
    }*/
} //php artisan test
