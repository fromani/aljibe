<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CodeGenerator config overrides
    |--------------------------------------------------------------------------
    |
    | It is a good idea to sperate your configuration form the code-generator's
    | own configuration. This way you won't lose any settings/preference
    | you have when upgrading to a new version of the package.
    |
    | Additionally, you will always know any the configuration difference between
    | the default config than your own.
    |
    | To override the setting that is found in the codegenerator.php file, you'll
    | need to create identical key here with a different value
    |
    | IMPORTANT: When overriding an option that is an array, the configurations
    | are merged together using php's array_merge() function. This means that
    | any option that you list here will take presence during a conflict in keys.
    |
    | EXAMPLE: The following addition to this file, will add another entry in
    | the common_definitions collection
    |
    |   'common_definitions' =>
    |   [
    |       [
    |           'match' => '*_at',
    |           'set' => [
    |               'css-class' => 'datetime-picker',
    |           ],
    |       ],
    |   ],
    |
     */

    /*
    |--------------------------------------------------------------------------
    | The default path of where the uploaded files live.
    |--------------------------------------------------------------------------
    |
    | You can use Laravel Storage filesystem. By default, the code-generator
    | uses the default file system.
    | For more info about Laravel's file system visit
    | https://laravel.com/docs/5.5/filesystem
    |
     */
    'files_upload_path' => 'uploads',

    'common_definitions' =>
    [
        [
            'match' => ['titulo*', '*nombre*', 'apellido', 'razon*social'],
            'set' => [
                'is-nullable' => false,
                'data-type' => 'string',
                'data-type-params' => [120],
            ],
        ],
        [
            'match' => ['numero_*', 'dni', 'cuit', 'cuil', 'cbu'],
            'set' => [
                'is-nullable' => false,
                'data-type' => 'biginteger',
            ],
        ],
        [
            'match' => ['periodo*', 'codigo*'],
            'set' => [
                'is-nullable' => true,
                'data-type' => 'integer',
            ],
        ],
        [
            'match' => ['comentario', 'observacion', 'descripcion'],
            'set' => [
                'is-nullable' => true,
                'data-type' => 'string',
                'data-type-params' => [255],
            ],
        ],
        [
            'match' => ['cantidad', 'total', 'importe', 'precio', 'costo', 'valor', 'porcentaje'],
            'set' => [
                'html-type' => 'number',
                'data-type' => 'decimal',
                'data-type-params' => [12,2],
            ],
        ],
        [
            'match' => ['fecha*'],
            'set' => [
                'data-type' => 'date',
            ],
        ],
        [
            'match' => '*ed_by',
            'set' => [
                'title' => 'User',
                'data-type' => 'integer',
                'foreign-relation' => [
                    'name' => 'creator',
                    'type' => 'belongsTo',
                    'params' => [
                        'App\\Domains\\Auth\\Models\\User',
                        'created_by',
                    ],
                    'field' => 'name',
                ],
                'on-store' => 'Illuminate\Support\Facades\Auth::Id();',
            ],
        ]
    ],

];
