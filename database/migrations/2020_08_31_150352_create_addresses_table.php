<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->string('localidad', 120);
            $table->string('codigo_postal', 10)->nullable();
            $table->string('direccion', 120);            
            $table->integer('distributor_id')->nullable();
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->integer('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->integer('reseller_id')->nullable();
            $table->foreign('reseller_id')->references('id')->on('resellers');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
