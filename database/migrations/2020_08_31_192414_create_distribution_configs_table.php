<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributionConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_configs', function (Blueprint $table) {
            $table->id();
            $table->integer('frecuencia')->unsigned()->comment('frecuencia de reparto en días');
            $table->integer('product_id');
            $table->foreign('product_id')->references('id')->on('productos')->onDelete('cascade');
            $table->integer('cantidad')->unsigned()->default(1);
            $table->integer('address_id');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->date('fecha_inicio');
            $table->date('suspended_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->unique(['product_id','address_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribution_configs');
    }
}
