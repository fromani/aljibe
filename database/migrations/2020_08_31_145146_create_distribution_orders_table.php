<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributionOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribution_orders', function (Blueprint $table) {
            $table->id();
            $table->string('numero', 20);
            $table->date('fecha');
            $table->integer('distributor_id');
            $table->foreign('distributor_id')->references('id')->on('distributors');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribution_orders');
    }
}
