<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlicuotaIvasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alicuotas_iva', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cod_afip')->unsigned()->unique();
            $table->string('descripcion', 30);
            $table->decimal('porcentaje', 5, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alicuotas_iva');
    }
}
