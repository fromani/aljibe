<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configservices', function(Blueprint $table)
        {
            $table->id();
            $table->integer('cantidad')->unsigned()->default(1);       
            $table->date('fecha_inicio');
            $table->date('fecha_proxima');
            $table->date('suspended_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreignId('service_plan_id')
                ->constrained('service_plans')
                ->onDelete('cascade');
            $table->foreignId('address_id')
                ->constrained('addresses')
                ->onDelete('cascade');
            $table->unique(['service_plan_id','address_id']);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configservices');
    }
}
