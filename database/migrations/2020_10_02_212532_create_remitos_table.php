<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRemitosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remitos', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('fecha_estimada');
            $table->date('fecha_real')->nullable();
            $table->foreignId('product_id')->unsigned()->constrained('productos');
            $table->foreignId('address_id')->unsigned()->constrained('addresses');
            $table->decimal('cantidad', 12,2);
            $table->decimal('precio_unitario', 12,2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('remitos');
    }
}
