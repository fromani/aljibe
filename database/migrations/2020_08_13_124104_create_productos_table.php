<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 120);            
            $table->string('codigo', 30)->unique()->comment('codigo de barras');
            $table->decimal('precio_actual', 12, 2)->default(0)->comment('precio de venta');
            $table->decimal('stock_minimo', 12, 2)->default(0);
            $table->decimal('stock_inventario', 12, 2)->default(1)->comment('cantidad en inventario');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('unidad_id')->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->integer('alicuota_iva_id')->unsigned();
            $table->foreign('alicuota_iva_id')->references('id')->on('alicuotas_iva');
            $table->boolean('es_retornable')->default(false);
            $table->string('color', 12)->nullable()->comment('ayuda a identificar visualmente el producto');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
