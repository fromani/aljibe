<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sevice_configs', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad')->unsigned()->default(1);       
            $table->date('fecha_inicio');
            $table->date('suspended_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('service_plan_id');
            $table->foreign('service_plan_id')->references('id')->on('service_plans')->onDelete('cascade');
            $table->integer('address_id');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
            $table->unique(['service_plan_id','address_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sevice_configs');
    }
}
