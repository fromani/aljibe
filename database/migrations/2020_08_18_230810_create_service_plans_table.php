<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_plans', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 120);
            $table->integer('periodo')->unsigned()->comment('es la cantidad del tipo de periodo');
            $table->string('tipo_periodo', 30)->comment('periodo de vencimiento: days = días, weeks = semanas, months = meses');
            $table->decimal('precio_actual', 12, 2)->default(0)->comment('precio periodico del servicio');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_plans');
    }
}
