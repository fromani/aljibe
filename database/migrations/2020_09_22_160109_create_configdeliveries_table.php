<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigdeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configdeliveries', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->foreignId('address_id')->constrained('addresses')->onDelete('cascade');
            $table->foreignId('product_id')->constrained('productos')->onDelete('cascade');
            $table->integer('frecuencia')->unsigned();
            $table->integer('cantidad')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_proxima');
            $table->timestamp('suspended_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configdeliveries');
    }
}
