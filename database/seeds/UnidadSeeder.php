<?php

use Illuminate\Database\Seeder;
use App\Unidad;

class UnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Unidad::truncate();

        Unidad::create([
        	'nombre' => 'Unidad',
        	'nombre_corto' => 'unid',
        	'cod_afip' => 1,
        ]);
        Unidad::create([
        	'nombre' => 'Kilogramos',
        	'nombre_corto' => 'kg',
        	'cod_afip' => 1,
        ]);
        Unidad::create([
        	'nombre' => 'Metros',
        	'nombre_corto' => 'm',
        	'cod_afip' => 2,
        ]);
        Unidad::create([
        	'nombre' => 'Metros Cuadrados',
        	'nombre_corto' => 'm2',
        	'cod_afip' => 3,
        ]);
        Unidad::create([
        	'nombre' => 'Metros Cúbicos',
        	'nombre_corto' => 'm3',
        	'cod_afip' => 4,
        ]);
        Unidad::create([
        	'nombre' => 'Litros',
        	'nombre_corto' => 'l',
        	'cod_afip' => 5,
        ]);
        Unidad::create([
        	'nombre' => 'Gramos',
        	'nombre_corto' => 'g',
        	'cod_afip' => 14,
        ]);
        Unidad::create([
        	'nombre' => 'Litros',
        	'nombre_corto' => 'ml',
        	'cod_afip' => 47,
        ]);
    }
}
