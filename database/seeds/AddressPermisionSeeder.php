<?php
  use App\Domains\Auth\Models\Permission;
  use Illuminate\Database\Seeder;
  use App\Domains\Auth\Models\User;

  class AddressPermisionSeeder extends Seeder
  {
    use DisableForeignKeys;
    public function run()
    {
      $this->disableForeignKeys();
      $address = Permission::create([
        "type" => User::TYPE_USER,
        "name" => "address.access",
        "description" => "Acceso a address"
      ]);

      $address->children()->saveMany([
        new Permission([
            "type" => User::TYPE_USER,
            "name" => "address.show",
            "description" => "Consultar address",
            "sort" => 1,
        ]),
        new Permission([
            "type" => User::TYPE_USER,
            "name" => "address.create",
            "description" => "Agregar address",
            "sort" => 2,
        ]),
        new Permission([
            "type" => User::TYPE_USER,
            "name" => "address.edit",
            "description" => "Modificar datos de address",
            "sort" => 3,
        ]),
        new Permission([
            "type" => User::TYPE_USER,
            "name" => "address.delete",
            "description" => "Eliminar address",
            "sort" => 4,
        ]),
      ]);
    }
  }