<?php
use App\Domains\Auth\Models\Permission;
use Illuminate\Database\Seeder;
use App\Domains\Auth\Models\User;

class ConfigDeliveryPermissionsSeeder extends Seeder
{
  use DisableForeignKeys;
  public function run()
  {
    $this->disableForeignKeys();
    $configdelivery = Permission::create([
      "type" => User::TYPE_USER,
      "name" => "configdelivery.access",
      "description" => "Acceso a configdelivery"
    ]);

    $configdelivery->children()->saveMany([
      new Permission([
          "type" => User::TYPE_USER,
          "name" => "configdelivery.show",
          "description" => "Consultar configdelivery",
          "sort" => 1,
      ]),
      new Permission([
          "type" => User::TYPE_USER,
          "name" => "configdelivery.create",
          "description" => "Agregar configdelivery",
          "sort" => 2,
      ]),
      new Permission([
          "type" => User::TYPE_USER,
          "name" => "configdelivery.edit",
          "description" => "Modificar datos de configdelivery",
          "sort" => 3,
      ]),
      new Permission([
          "type" => User::TYPE_USER,
          "name" => "configdelivery.delete",
          "description" => "Eliminar configdelivery",
          "sort" => 4,
      ]),
    ]);
  }
}