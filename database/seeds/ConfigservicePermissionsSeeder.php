<?php
                  use App\Domains\Auth\Models\Permission;
                  use Illuminate\Database\Seeder;
                  use App\Domains\Auth\Models\User;
            
                  class ConfigservicePermissionsSeeder extends Seeder
                  {
                    use DisableForeignKeys;
                    public function run()
                    {
                      $this->disableForeignKeys();
                      $configservice = Permission::create([
                        "type" => User::TYPE_USER,
                        "name" => "configservice.access",
                        "description" => "Acceso a configservice"
                      ]);

                      $configservice->children()->saveMany([
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "configservice.show",
                            "description" => "Consultar configservice",
                            "sort" => 1,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "configservice.create",
                            "description" => "Agregar configservice",
                            "sort" => 2,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "configservice.edit",
                            "description" => "Modificar datos de configservice",
                            "sort" => 3,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "configservice.delete",
                            "description" => "Eliminar configservice",
                            "sort" => 4,
                        ]),
                      ]);
                    }
                  }