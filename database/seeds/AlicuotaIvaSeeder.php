<?php

use Illuminate\Database\Seeder;
use App\AlicuotaIva;

class AlicuotaIvaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AlicuotaIva::truncate();

        AlicuotaIva::create([
        	'cod_afip' => 1,
            'descripcion' => 'No Gravado',
            'porcentaje' => null,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 2,
            'descripcion' => 'Exento',
            'porcentaje' => null,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 3,
            'descripcion' => '0%',
            'porcentaje' => 0,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 9,
            'descripcion' => '2.50%',
            'porcentaje' => 2.5,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 8,
            'descripcion' => '5%',
            'porcentaje' => 5,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 4,
            'descripcion' => '10.50%',
            'porcentaje' => 10.5,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 5,
            'descripcion' => '21%',
            'porcentaje' => 21,
        ]);
        AlicuotaIva::create([
        	'cod_afip' => 6,
            'descripcion' => '27%',
            'porcentaje' => 27,
        ]);
    }
}
