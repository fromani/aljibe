<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'activity_log',
            'failed_jobs',
        ]);

        //$this->call(AuthSeeder::class);
        //$this->call(AnnouncementSeeder::class);
        //$this->call(AddressPermisionSeeder::class);
        //$this->call(AlicuotaIvaSeeder::class);
        //$this->call(CategorySeeder::class);
        //$this->call(PermissionsSeeder::class);
        //$this->call(ConfigDeliveryPermissionsSeeder::class);
        //$this->call(ConfigservicePermissionsSeeder::class);

        Model::reguard();
    }
}
