<?php
                  use App\Domains\Auth\Models\Permission;
                  use Illuminate\Database\Seeder;
                  use App\Domains\Auth\Models\User;
            
                  class CustomerSeederPermissions extends Seeder
                  {
                    use DisableForeignKeys;
                    public function run()
                    {
                      $this->disableForeignKeys();
                      $customer = Permission::create([
                        "type" => User::TYPE_USER,
                        "name" => "customer.access",
                        "description" => "Acceso a customer"
                      ]);

                      $customer->children()->saveMany([
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "customer.show",
                            "description" => "Consultar customer",
                            "sort" => 1,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "customer.create",
                            "description" => "Agregar customer",
                            "sort" => 2,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "customer.edit",
                            "description" => "Modificar datos de customer",
                            "sort" => 3,
                        ]),
                        new Permission([
                            "type" => User::TYPE_USER,
                            "name" => "customer.delete",
                            "description" => "Eliminar customer",
                            "sort" => 4,
                        ]),
                      ]);
                    }
                  }