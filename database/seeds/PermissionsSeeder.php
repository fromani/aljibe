<?php

use App\Domains\Auth\Models\Permission;
use App\Domains\Auth\Models\Role;
use App\Domains\Auth\Models\User;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
	use DisableForeignKeys;
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $category = Permission::create([
            'type' => User::TYPE_USER,
            'name' => 'category.access',
            'description' => 'All Categories Permissions',
        ]);

        $category->children()->saveMany([
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'category.show',
                'description' => 'View Categories',
                'sort' => 1,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'category.create',
                'description' => 'Add Categories',
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'category.edit',
                'description' => 'Edit Data Categories',
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'category.delete',
                'description' => 'Delete Categories',
                'sort' => 4,
            ]),
        ]);

        $product = Permission::create([
            'type' => User::TYPE_USER,
            'name' => 'product.access',
            'description' => 'All Products Permissions',
        ]);

        $product->children()->saveMany([
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'product.show',
                'description' => 'View Products',
                'sort' => 5,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'product.create',
                'description' => 'Add Products',
                'sort' => 6,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'product.edit',
                'description' => 'Edit Data Products',
                'sort' => 7,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'product.delete',
                'description' => 'Delete Products',
                'sort' => 8,
            ]),
        ]);

        $serviceplan = Permission::create([
            'type' => User::TYPE_USER,
            'name' => 'serviceplan.access',
            'description' => 'All Service Plans Permissions',
        ]);

        $serviceplan->children()->saveMany([
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'serviceplan.show',
                'description' => 'View Service Plans',
                'sort' => 1,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'serviceplan.create',
                'description' => 'Add Service Plans',
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'serviceplan.edit',
                'description' => 'Edit Data Service Plans',
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'serviceplan.delete',
                'description' => 'Delete Service Plans',
                'sort' => 4,
            ]),
        ]);
        
        $distributor = Permission::create([
            'type' => User::TYPE_USER,
            'name' => 'distributor.access',
            'description' => 'All Distributors Permissions',
        ]);

        $distributor->children()->saveMany([
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'distributor.show',
                'description' => 'View Distributors',
                'sort' => 1,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'distributor.create',
                'description' => 'Add Distributors',
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'distributor.edit',
                'description' => 'Edit Data Distributors',
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'distributor.delete',
                'description' => 'Delete Distributors',
                'sort' => 4,
            ]),
        ]);

        $customer = Permission::create([
            'type' => User::TYPE_USER,
            'name' => 'customer.access',
            'description' => 'All customers Permissions',
        ]);

        $customer->children()->saveMany([
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'customer.show',
                'description' => 'View customers',
                'sort' => 1,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'customer.create',
                'description' => 'Add customers',
                'sort' => 2,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'customer.edit',
                'description' => 'Edit Data customers',
                'sort' => 3,
            ]),
            new Permission([
                'type' => User::TYPE_USER,
                'name' => 'customer.delete',
                'description' => 'Delete customers',
                'sort' => 4,
            ]),
        ]);
        // Assign Permissions to other Roles
        //

        $this->enableForeignKeys();
    }
}
