<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Distriutor;
use Faker\Generator as Faker;

$factory->define(Distriutor::class, function (Faker $faker) {
    return [
    	'nombre' => $faker->name, 
    	'telefono' => $faker->phoneNumber, 
    	'localidad' => $faker->city, 
    	'created_by' => 2,
    ];
});