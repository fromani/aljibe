<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Distributor;
use Faker\Generator as Faker;

$factory->define(Distributor::class, function (Faker $faker) {
    return [
    	'nombre' => $faker->name, 
    	'telefono' => $faker->phoneNumber, 
    	'localidad' => $faker->city, 
    	'created_by' => 2,
    ];
});
