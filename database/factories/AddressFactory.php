<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'localidad' => $faker->city, 
        'codigo_postal'  => $faker->postcode, 
        'direccion'  	=> $faker->secondaryAddress, 
        'distributor_id'  => 1, 
        'customer_id' => 1, 
    ];
});
