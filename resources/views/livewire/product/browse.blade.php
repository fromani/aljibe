<div>
    @section('right-nav-items')
    	<div>
    		<ul class="nav">
	            <li class="nav-item">
		            <x-utils.link class="nav-link" :href="route('frontend.user.categories')" :text="__('Categories')" :icon="'nav-icon i-Split-Four-Square-Window'" :permission="'category.access'" />
	            </li>
	            <li class="nav-item">
	                <x-utils.link class="nav-link" :href="'#'" :text="__('Promotions')" :icon="'nav-icon i-Money-Bag'" :permission="'promotion.access'" />
	            </li>
	        </ul>
	    </div>
    @endsection
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	@include('livewire.product.includes.form')
	    </div>
		<div class="col-md-9 mb-3">
			@include('livewire.product.includes.filters')
			@include('livewire.product.includes.list')
		</div>
	</div>
</div>
