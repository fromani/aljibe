<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> {{ $form['title'] }} </h4>
        @if(session()->get('flash_success'))
		    <x-utils.alert type="success" class="header-message">
		        {{ session()->get('flash_success') }}
		    </x-utils.alert>
		@endif
		@if(session()->get('flash_warning'))
		    <x-utils.alert type="warning" class="header-message">
		        {{ session()->get('flash_warning') }}
		    </x-utils.alert>
		@endif
	    <form wire:submit.prevent="{{ $form['action'] }}">
		@csrf
			<x-utils.form-control 
	    		:label="'Categoría'">
	    		@slot('control')
	    		<select class="form-control" id="category_id" name="category_id" wire:model="category_id" required>
	    			@foreach($categories as $categ)
	    			<option value="{{ $categ->id }}">{{ $categ->nombre }}</option>
	    			@endforeach
	    		</select>
	    		@endslot
	    		@error('category_id')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
	    	<x-utils.form-control 
	    		:label="'Nombre de Producto'">
	    		@slot('control')
	    		<input type="text" class="form-control" id="nombre" name="nombre" wire:model.debounce.1s="nombre" required> 
	    		@endslot
	    		@error('nombre')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
	    	<x-utils.form-control 
	    		:label="'Código'">
	    		@slot('control')
	    		<input type="text" class="form-control" id="codigo" name="codigo" wire:model.debounce.1s="codigo" required> 
	    		@endslot
	    		@error('codigo')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
			<x-utils.form-control 
	    		:label="'Precio De Venta'">
	    		@slot('control')
	    		<div class="input-group">
					<div class="input-group-prepend">
				    	<span class="input-group-text" id="precio-addon2">$</span>
				  	</div>
				  	<input type="number" id="precio_actual" name="precio_actual" class="form-control" placeholder="Importe" aria-label="Precio de precio_actual" aria-describedby="precio-addon2" wire:model.debounce.1s="precio_actual" required>					  
				</div>
	    		@endslot
	    		@error('precio_actual')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>		    	
	    	<div class="form-row">
    			<div class="form-group col-md-6">
    				<x-utils.form-control 
			    		:label="'Unidad'">
			    		@slot('control')
			    		<select class="form-control" id="unidad_id" name="unidad_id" wire:model="unidad_id" required>
			    			@foreach($units as $unit)
			    			<option value="{{ $unit->id }}">{{ $unit->nombre }}</option>
			    			@endforeach
			    		</select>
			    		@endslot
			    		@error('unidad_id')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
			    	</x-utils.form-control>
    			</div>
    			<div class="form-group col-md-6">
    				<x-utils.form-control 
			    		:label="'Alícuota De IVA'">
			    		@slot('control')
			    		<select class="form-control" id="alicuota_iva_id" name="alicuota_iva_id" wire:model="alicuota_iva_id" required>
			    			@foreach($alicuotas as $alic)
			    			<option value="{{ $alic->id }}">{{ $alic->descripcion }}</option>
			    			@endforeach
			    		</select>
			    		@endslot
			    		@error('alicuota_iva_id')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
			    	</x-utils.form-control>
    			</div>
    		</div>
    		<div class="form-row">
    			<div class="form-group col-md-6">
		    		<x-utils.form-control>
			    		@slot('control')
			    		<label class="checkbox checkbox-outline-primary">
		                    <input type="checkbox" checked="checked" id="es_retornable" name="es_retornable" wire:model="es_retornable"><span>Retornable</span><span class="checkmark"></span>
		                </label>
			    		@endslot
			    		@error('codigo')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
			    	</x-utils.form-control>
			    </div>
			    <div class="form-group col-md-6">
			    	<x-utils.form-control>
			    		@slot('control')				    	
						<button title="actualizar" type="button" wire:click="refreshColor" class="btn btn-sm" style="background-color:{{$color}}">
							<i class="text-20 i-Shuffle-21"></i>
						</button>
						<div wire:loading wire:target="refreshColor">
					        Actualizando color...
					    </div>
						@endslot
			    		@error('codigo')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
			    	</x-utils.form-control>
			    </div>
			</div>
	    	<button type="submit" class="btn btn-primary btn-md" >{{ $form['title'] }}</button>
	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-1" type="button" wire:click="default">Cancelar</button>
	    	@endif
	    </form>
    </div>
</div>