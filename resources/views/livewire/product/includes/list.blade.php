<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de productos </h4>
	    <div class="table-responsive">
	        <table class="table">
	            <thead class="thead-dark">
	                <tr>
	                    <th scope="col">#</th>
	                    <th scope="col">Nombre</th>
	                    <th scope="col">Código</th>
	                    <th scope="col">Categoría</th>
	                    <th scope="col">Precio</th>
	                    <th scope="col">Retornable</th>
	                    <th scope="col">Estado</th>
	                    <th scope="col">Acción</th>
	                </tr>
	            </thead>
	            <tbody>
	            	@foreach($products as $prod)
	                <tr @if($this->editing($prod->id))class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$prod->id}}</th>
	                    <td><span class="badge mr-2 pr-2" style="background-color:{{$prod->color}}">&nbsp;</span>{{$prod->nombre}}</td>
	                    <td>{{$prod->codigo}}</td>
	                    <td>{{$prod->categoria->nombre}}</td>
	                    <td align="right">@money($prod->precio)</td>
	                    <td>{!! $prod->retornable !!}</td>
	                    <td>{!! $prod->estado !!}</td>
	                    <td>			                    	
	                    	@if($prod->deleted_at)
	                    	<button class="btn btn-warning mr-2" wire:click="restore({{ $prod->id }})"><i class="nav-icon i-Restore-Window font-weight-bold"></i></button>
	                    	@else
	                    	<button class="btn btn-success mr-2" wire:click="edit({{ $prod->id }})"><i class="nav-icon i-Pen-2 font-weight-bold"></i></button>
	                    	<button class="btn btn-danger mr-2" wire:click="destroy({{ $prod->id }})"><i class="nav-icon i-Close-Window font-weight-bold"></i></button>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
	            </tbody>
	            <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
	        </table>
	        {{ $products->links() }}
	    </div>
	</div>
</div>