<div class="card mb-3">
    <div class="card-body">
        <h4 class="card-title mb-3"> Filtros </h4>
        <label class="switch pr-3 switch-success mr-2"><span>Activos</span>
            <input type="checkbox" checked="checked" wire:model="filter_active"><span class="slider"></span>
        </label>
        <label class="switch pr-3 switch-danger mr-2"><span>Eliminados</span>
            <input type="checkbox" checked="checked" wire:model="filter_removed"><span class="slider"></span>
        </label>
        <label class="switch pr-3 switch-info mr-2"><span>Retornable</span>
            <input type="checkbox" checked="checked" wire:model="filter_retornable"><span class="slider"></span>
        </label>
        <label class="switch pr-3 switch-warning mr-2"><span>No Retornable</span>
            <input type="checkbox" checked="checked" wire:model="filter_noretornable"><span class="slider"></span>
        </label>
        <label class="switch pr-3 switch-warning mr-2"><span>{{ __('Category') }}</span>
	        <select class="form-control" id="filter_category" name="filter_category" wire:model="filter_category">
	        	<option value="">-- TODAS --</option>
				@foreach($categories as $categ)
				<option value="{{ $categ->id }}">{{ $categ->nombre }}</option>
				@endforeach
			</select>
		</label>
    </div>
</div>