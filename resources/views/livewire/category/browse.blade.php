<div class="row">
	<div class="col-md-3 mb-3">
        @include('livewire.category.includes.form')
        @include('livewire.category.includes.filters')
    </div>
	<div class="col-md-9 mb-3">
	    @include('livewire.category.includes.list')
	</div>
</div>
