<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> {{ $form['title'] }} </h4>
        @if(session()->get('flash_success'))
		    <x-utils.alert type="success" class="header-message">
		        {{ session()->get('flash_success') }}
		    </x-utils.alert>
		@endif
		@if(session()->get('flash_warning'))
		    <x-utils.alert type="warning" class="header-message">
		        {{ session()->get('flash_warning') }}
		    </x-utils.alert>
		@endif
	    <form wire:submit.prevent="{{ $form['action'] }}">
		@csrf
	    	<x-utils.form-control 
	    		:label="'Nombre de Categoría'">
	    		@slot('control')
	    		<input type="text" class="form-control" id="nombre" name="nombre" wire:model.debounce.1s="nombre" required> 
	    		@endslot
	    		@error('nombre')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
	    	<button type="submit" class="btn btn-primary btn-md" >{{ $form['title'] }}</button>
	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-1" type="button" wire:click="default">Cancelar</button>
	    	@endif
	    </form>
    </div>
</div>