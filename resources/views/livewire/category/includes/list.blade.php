<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de categorías </h4>
	    <div class="table-responsive">
	        <table class="table">
	            <thead class="thead-dark">
	                <tr>
	                    <th scope="col">#</th>
	                    <th scope="col">Nombre</th>
	                    <th scope="col">Estado</th>
	                    <th scope="col">Acción</th>
	                </tr>
	            </thead>
	            <tbody>
	            	@foreach($categories as $categ)
	                <tr @if($this->editing($categ->id))class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$categ->id}}</th>
	                    <td>{{$categ->nombre}}</td>
	                    <td>{!! $categ->estado !!}</td>
	                    <td>			                    	
	                    	@if($categ->deleted_at)
	                    	<button class="btn btn-warning mr-2" wire:click="restore({{ $categ->id }})"><i class="nav-icon i-Restore-Window font-weight-bold"></i></button>
	                    	@else
	                    	<button class="btn btn-success mr-2" wire:click="edit({{ $categ->id }})"><i class="nav-icon i-Pen-2 font-weight-bold"></i></button>
	                    	<button class="btn btn-danger mr-2" wire:click="destroy({{ $categ->id }})"><i class="nav-icon i-Close-Window font-weight-bold"></i></button>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
	            </tbody>
	        </table>
	        {{ $categories->links() }}
	    </div>
	</div>
</div>