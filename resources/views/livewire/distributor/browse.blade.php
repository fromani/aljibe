<div>
    @section('right-nav-items')
    	<div>
    		<ul class="nav">
	            <li class="nav-item">
		            <x-utils.link class="nav-link" 
		            	:href="'#'" 
		            	:text="__('Addresses')" 
		            	:icon="'nav-icon i-Map2'" 
		            	:permission="'address.access'" />
	            </li>
	            <li class="nav-item">
	                <x-utils.link class="nav-link" :href="'#'" :text="__('Distribution Orders')" :icon="'nav-icon i-Jeep'" :permission="'distribution-order.access'" />
	            </li>
	        </ul>
	    </div>
    @endsection
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	@include('livewire.distributor.includes.form')
	    </div>
		<div class="col-md-9 mb-3">
			@include('livewire.distributor.includes.filters')
			@include('livewire.distributor.includes.list')
		</div>
	</div>
</div>
