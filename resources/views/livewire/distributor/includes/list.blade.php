<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de repartidores </h4>
	    <div class="table-responsive">
	        <table class="table">
	            <thead class="thead-dark">
	                <tr>
	                    <th scope="col">#</th>
	                    <th scope="col">Nombre</th>
	                    <th scope="col">Localidad</th>
	                    <th scope="col">Telefono</th>
	                    <th scope="col">Estado</th>
	                    <th scope="col">Acción</th>
	                </tr>
	            </thead>
	            <tbody>
	            	@foreach($distributors as $dist)
	            	
	                <tr @if($dist->id == $keyid)class="font-weight-bold bg-light text-dark" @endif>
	                	<th scope="row">{{$dist->id}}</th>
	                    <td>{{$dist->nombre}}</td>
	                    <td>{{$dist->localidad}} </td>
	                    <td>{{$dist->telefono}} </td>
	                    <td>{!! $dist->estado !!}</td>
	                    <td>			                    	
	                    	@if($dist->deleted_at)
	                    	<x-action.restore wire:click="restore({{ $dist->id }})"/>
	                    	@else
	                    	<x-action.edit wire:click="edit({{ $dist->id }})"/>
	                    	<x-action.delete wire:click="destroy({{ $dist->id }})"/>
	                    	@endif			                    	
	                    </td>
	                </tr>
	               	@endforeach
	            </tbody>
	            <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
	        </table>
	        {{ $distributors->links() }}
	    </div>
	</div>
</div>