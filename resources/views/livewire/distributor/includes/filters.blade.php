<div class="card mb-3">
    <div class="card-body">
        <h4 class="card-title mb-3"> Filtros </h4>
        <form class="form-inline" wire:submit.prevent>
  			<div class="form-group">
  				<label for="filter_city" class="mx-2">Localidad:  </label>
    			<input class="form-control" type="text" name="filter_city" id="filter_city" wire:model.debounce.1s="filter_city"/>
  			</div>
  			<div class="checkbox">
  				<label class="switch pr-3 switch-success mr-2"><span>Activos</span>
		            <input type="checkbox" checked="checked" wire:model="filter_active"><span class="slider"></span>
		        </label>
  			</div>
  			<div class="checkbox">
  				<label class="switch pr-3 switch-danger mr-2"><span>Eliminados</span>
		            <input type="checkbox" checked="checked" wire:model="filter_removed"><span class="slider"></span>
		        </label>
  			</div>
  		</form> 
    </div>
</div>