<div>
    <x-utils.message/>
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	@include("{$includes}.form")
	    </div>
		<div class="col-md-9 mb-3">
			@include("{$includes}.list")
		</div>
	</div>
</div>
