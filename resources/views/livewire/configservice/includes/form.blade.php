<x-frontend.card class="mt-2">
    <x-slot name="header">
        <h4 class="card-title my-2"> {{ $form['title'] }} </h4>
    </x-slot>

    <x-slot name="body">       
        <x-forms.post wire:submit.prevent="{{ $form['action'] }}">

            <x-input.select name="service_plan_id" :label="'Servicio'" wire:model.defer="service_plan_id">
                <x-slot name="options">
                    <option value="">Seleccione uno</option>
                    @foreach($services as $service)
                        <option value="{{ $service->id }}">{{ $service->nombre }}</option>
                    @endforeach
                </x-slot>
            </x-input.select>

            <x-input.number name="cantidad" id="cantidad" :label="'Cantidad'" wire:model.defer="cantidad" placeholder="cantidad de servicios" required/>

            <x-input.date name="fecha_inicio" id="fecha_inicio" :label="'Fecha de inicio'" wire:model.defer="fecha_inicio" required/>

            @if($this->editing($keyid))
            <br>
            <x-input.switch name="suspended" id="suspended" label="Suspendido" wire:model="suspended" switchclass="danger"/>
            <br>
            @endif
            <x-button.save label="Guardar" />

	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-2" type="button" wire:click="default">Cancelar</button>
	    	@endif

        </x-forms.post>
    </x-slot>
</x-frontend.card>