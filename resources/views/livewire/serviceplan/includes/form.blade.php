<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> {{ $form['title'] }} </h4>
        @if(session()->get('flash_success'))
		    <x-utils.alert type="success" class="header-message">
		        {{ session()->get('flash_success') }}
		    </x-utils.alert>
		@endif
		@if(session()->get('flash_warning'))
		    <x-utils.alert type="warning" class="header-message">
		        {{ session()->get('flash_warning') }}
		    </x-utils.alert>
		@endif
	    <form wire:submit.prevent="{{ $form['action'] }}">
		@csrf
			<x-utils.form-control 
	    		:label="'Nombre de Producto'">
	    		@slot('control')
	    		<input type="text" class="form-control" id="nombre" name="nombre" wire:model.debounce.1s="nombre" required> 
	    		@endslot
	    		@error('nombre')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
	    	<x-utils.form-control 
	    		:label="'Período de Vto.'">
	    		@slot('control')
	    		<div class="row">
	    			<div class="col col-md-3 pr-1">
		    			<input type="number" class="form-control" placeholder="cantidad" id="periodo" name="periodo" wire:model.debounce.1s="periodo" required>
		    		</div>
	    			<div class="col col-md-9 ml-0 pl-0">
	    				<select class="form-control" id="tipo_periodo" name="tipo_periodo" wire:model="tipo_periodo" required>
			    			@foreach($tipos_periodo as $tipo_periodoKey => $tipo_periodoNombre)
			    			<option value="{{ $tipo_periodoKey }}">{{ $tipo_periodoNombre }}</option>
			    			@endforeach
			    		</select>
	    			</div>
	    		</div>		    		
	    		@endslot
	    		@error('periodo')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>
	    	<x-utils.form-control 
	    		:label="'Precio Del Servicio'">
	    		@slot('control')
	    		<div class="input-group">
					<div class="input-group-prepend">
				    	<span class="input-group-text" id="precio-addon2">$</span>
				  	</div>
				  	<input type="number" id="precio_actual" name="precio_actual" class="form-control" placeholder="Importe" aria-label="Precio de precio_actual" aria-describedby="precio-addon2" wire:model.debounce.1s="precio_actual" required>					  
				</div>
	    		@endslot
	    		@error('precio_actual')
					<small class="text-danger"> {{ $message }}</small>
				@enderror
	    	</x-utils.form-control>	
			@include('livewire.serviceplan.includes.productlist')
			<button type="submit" class="btn btn-primary btn-md m-2" >{{ $form['title'] }}</button>
	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-2" type="button" wire:click="default">Cancelar</button>
	    	@endif
		</form>
	</div>
</div>