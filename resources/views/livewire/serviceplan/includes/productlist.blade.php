<div>
	<ul class="list-group" id="accordionOne">
		<x:frontend.accordion :products="$products">
			<li class="list-group-item">
				<x-slot name="trigger">
		            <button type="button" class="btn btn-link">
		          		Incluir Productos <span class="mx-2"><i class="text-12" :class="{
				          'i-Arrow-Down-2': open,
				          'i-Arrow-Right-2': !open
				        }"></i></span>
			        </button>
		        </x-slot>
		    </li>

	    	<li class="list-group-item" style="background-color: gainsboro;">
	    		<div class="row">
	    			<div class="col col-md-9 mx-0 px-0">
				    	<select class="form-control" id="producto_id" name="producto_id" x-model="productId">
			    			<template x-for="(option, index) in optionsProducts" :key="index">
			    				<option :value="option.id" x-text="option.nombre"></option>
			    			</template>
			    		</select>
			    		@error('producto_id')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
					</div>
					<div class="col col-md-3 mx-0 px-0">
			    		<input type="number" id="cantidad" name="cantidad" class="form-control" placeholder="Cantidad" aria-label="Cantidad" x-model="cantidad" @keydown.enter="addProduct($dispatch)">
			    		@error('cantidad')
							<small class="text-danger"> {{ $message }}</small>
						@enderror
					</div>
				</div>
				<template x-if="cantidad>0">
		    		<button class="btn btn-secondary btn-sm my-2" type="button" @click="addProduct($dispatch)">Aceptar</button>
		    	</template>
		    	<div x-show.transition="!(cantidad>0)">
				    <small class="text-danger"> Debe completar los campos solicitados</small>
				</div>
		    </li>			    
		</x:frontend.accordion>

		<div 
			x-data="arrProduct"
			x-init="$watch('productNumber', value => $dispatch('input',products))"
			@notify-edit-service.window="initArray($event.detail.productsplan)" 
			@notify-add-product.window="addProduct($event.detail)" 
			wire:model="productsplan"
		>
			<template x-for="(product, index) in products" :key="index">
				<li class="list-group-item d-flex justify-content-between align-items-center">
					<button class="btn btn-danger btn-sm" type="button" @click="deleteProduct(index)">&cross;</button>
					<span x-text="product.nombre"></span>
					<span class="badge badge-primary badge-pill" x-text="product.pivot.cantidad"></span>
				</li>
			</template>
		</div>
	</ul>	
</div>
@push('after-scripts')
<script>
var arrProduct = {
	products: @json($productsplan),
	productNumber: -1,
	initArray(arr) {
		this.products = [];
		this.products = arr;
	},
	existProduct(prodObj) {
    	let result = this.products.find(function(elem, index) {
    		let existe = false;
    		// si el producto existe, le suma la cantidad
		  	if (parseInt(elem.id) === parseInt(prodObj.id)){
		  		elem.pivot.cantidad = parseInt(elem.pivot.cantidad) + parseInt(prodObj.pivot.cantidad);
		  		existe = true;
		  	}
		  	return existe;
		});
		return result !== undefined; 
    },
    addProduct(objProduct) {
    	if(!this.existProduct(objProduct)) {
    		this.products.push(objProduct);
    		this.productNumber = this.products.length;
    	}
    },
    deleteProduct(index) {
        this.products = this.products.filter((product, productIndex) => {
            return index !== productIndex
        });
        this.productNumber = this.products.length;
    }
};
</script>
@endpush