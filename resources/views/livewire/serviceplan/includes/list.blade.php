<div class="card">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de servicios </h4>
	    <div class="table-responsive">
	        <table class="table">
	            <thead class="thead-dark">
	                <tr>
	                    <th scope="col">#</th>
	                    <th scope="col">Nombre</th>
	                    <th scope="col">Periodo</th>
	                    <th scope="col">Productos</th>
	                    <th scope="col">Precio</th>
	                    <th scope="col">Estado</th>
	                    <th scope="col">Acción</th>
	                </tr>
	            </thead>
	            <tbody>
	            	@foreach($services as $serv)
	            	
	                <tr @if($serv->id == $keyid)class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$serv->id}}</th>
	                    <td><span class="badge mr-2 pr-2" style="background-color:{{$serv->color}}">&nbsp;</span>{{$serv->nombre}}</td>
	                    <td>{{$serv->periodo}} @lang($serv->tipo_periodo)</td>
	                    <td>{{ $serv->products->count() }}</td>
	                    <td align="right">@money($serv->precio)</td>
	                    <td>{!! $serv->estado !!}</td>
	                    <td>			                    	
	                    	@if($serv->deleted_at)
	                    	<button class="btn btn-warning mr-2" wire:click="restore({{ $serv->id }})"><i class="nav-icon i-Restore-Window font-weight-bold"></i></button>
	                    	@else
	                    	<button class="btn btn-success mr-2" wire:click="edit({{ $serv->id }})"><i class="nav-icon i-Pen-2 font-weight-bold"></i></button>
	                    	<button class="btn btn-danger mr-2" wire:click="destroy({{ $serv->id }})"><i class="nav-icon i-Close-Window font-weight-bold"></i></button>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
	            </tbody>
	            <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
	        </table>
	        {{ $services->links() }}
	    </div>
	</div>
</div>