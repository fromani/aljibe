<div class="card mb-3">
    <div class="card-body">
        <h4 class="card-title mb-3"> Filtros </h4>
        <label class="switch pr-3 switch-success mr-2"><span>Activos</span>
            <input type="checkbox" checked="checked" wire:model="filter_active"><span class="slider"></span>
        </label>
        <label class="switch pr-3 switch-danger mr-2"><span>Eliminados</span>
            <input type="checkbox" checked="checked" wire:model="filter_removed"><span class="slider"></span>
        </label>
    </div>
</div>