<div>
	<div class="row">
	    <div class="col-md-3 mb-3">
	    	@include('livewire.serviceplan.includes.form')
	    </div>
		<div class="col-md-9 mb-3">
			@include('livewire.serviceplan.includes.filters')
			@include('livewire.serviceplan.includes.list')
		</div>
	</div>
</div>
