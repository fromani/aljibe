<x-frontend.card class="mt-2">
    <x-slot name="header">
        <h4 class="card-title my-2"> {{ $form['title'] }} </h4>
    </x-slot>

    <x-slot name="body">       
        <x-forms.post wire:submit.prevent="{{ $form['action'] }}">

            <x-input.text name="localidad" id="localidad" :label="'Localidad'" wire:model.defer="localidad" required/>

            <x-input.number name="codigo_postal" id="codigo_postal" :label="'Código Postal'" wire:model.defer="codigo_postal" placeholder="dígitos del CP"/>

            <x-input.text name="direccion" id="direccion" :label="'Dirección'" wire:model.defer="direccion" required/>

            <x-input.select name="reseller_id" :label="'Revendedor'" wire:model.defer="reseller_id">
                <x-slot name="options">
                    <option value="">Seleccione uno</option>
                    @foreach($resellers as $reseller)
                        <option value="{{ $reseller->id }}">{{ $reseller->nombre }}</option>
                    @endforeach
                </x-slot>
            </x-input.select>

            <x-input.select name="distributor_id" :label="'Repartidor'" wire:model.defer="distributor_id">
                <x-slot name="options">
                    <option value="">Seleccione uno</option>
                    @foreach($distributors as $distributor)
                        <option value="{{ $distributor->id }}">{{ $distributor->nombre }}</option>
                    @endforeach
                </x-slot>
            </x-input.select>

            <x-button.save label="Guardar" />

	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-2" type="button" wire:click="default">Cancelar</button>
	    	@endif

        </x-forms.post>
    </x-slot>
</x-frontend.card>