<div 
  x-data
  id="modalLive" 
  class="modal fade show"
  tabindex="-1" 
  role="dialog" 
  aria-labelledby="modalLiveLabel" 
  style="display: block; padding-right: 17px;background-color: #3f42448a;"      
>    
  <div 
    class="modal-dialog modal-lg"
    style="max-width: 90%" 
    role="document"
    id="modalContent"
    x-on:click.away="$wire.closemodal()"
  >
    <div class="modal-content">          
      <div class="modal-header">
        <h5 class="modal-title" id="modalLiveLabel">CONFIGURACIONES / {{$customer->nombre}} / {{ $data_model->direccion }}, {{ $data_model->localidad }} (#{{ $data_model->id }})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="$emitUp('modalClose')">
          <span aria-hidden="true">x</span>
        </button>
      </div>
      <div class="modal-body"> 

        <div x-data="{ tab: window.location.hash ? window.location.hash.substring(1) : 'configdelivery' }" id="tab_wrapper">
        <!-- The tabs navigation -->
          <ul class="nav nav-tabs">

            <li class="nav-item badge-top-container"><a :class="{'nav-link active': tab === 'configdelivery','nav-link': tab != 'configdelivery'}" @click.prevent="tab = 'configdelivery'; window.location.hash = 'configdelivery'" href="#"><i class="fas fa-truck"></i> Configuración de Repartos <span class="badge badge-pill badge-primary">{{ $data_model->configdeliveries()->count() }}</span></a> </li>

            <li class="nav-item badge-top-container"><a :class="{'nav-link active': tab === 'configservices','nav-link': tab != 'configservices'}" @click.prevent="tab = 'configservices'; window.location.hash = 'configservices'" href="#"><i class="fas fa-toolbox"></i> Configuración de Servicios <span class="badge badge-pill badge-primary">{{ $data_model->configservices()->count() }}</span></a></li>

          </ul>

        <!-- The tabs content -->
          <div x-show="tab === 'configdelivery'">
            @livewire('configdelivery.configdelivery-component', ['address' => $data_model], key('configdeliveries_'.$data_model->id))
          </div>
          <div x-show="tab === 'configservices'">
            @livewire('configservice.configservice-component', ['address' => $data_model], key('configservices_'.$data_model->id))
          </div>
        </div>           
        
      </div>   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="$emitUp('modalClose')">Cerrar</button>
      </div>       
    </div>
  </div>      
</div> 