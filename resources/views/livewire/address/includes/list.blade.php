<div class="card mt-2">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de Domicilios </h4>
	    <div class="table-responsive">
			<table class="table w-full">
			    <thead>
			        <tr class="bg-gray-600 text-white">
			            <th class="px-4 py-2" scope="col">#</th>
			            <th class="px-4 py-2" scope="col">Localidad</th>
			            <th class="px-4 py-2" scope="col">Dirección</th>
			            <th class="px-4 py-2" scope="col">Revendedor</th>
			            <th class="px-4 py-2" scope="col">Repartidor</th>
			            <th class="px-4 py-2" scope="col">Estado</th>
			            <th class="px-4 py-2" scope="col">acciones</th>
			        </tr>
			    </thead>
			    <tbody>
			    	@foreach($list_address as $address)
	                <tr @if($this->editing($address->id))class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$address->id}}</th>
	                    <td>{{$address->localidad}}</td>
	                    <td>{{$address->direccion}}</td>
	                    <td>{{ $address->reseller ? $address->reseller->nombre : '' }}</td>
	                    <td>{{ $address->distributor ? $address->distributor->nombre : '' }}</td>
	                    <td><h5>{!! $address->estado !!}</h5></td>
	                    <td class="d-flex justify-content-center">			                    	
	                    	@if($address->deleted_at)
	                    	<x-action.restore wire:click="restore({{ $address->id }})"/>
	                    	@else
	                    	<x-action.config wire:click="modalconfig({{ $address->id }})"/>
	                    	<x-action.edit wire:click="edit({{ $address->id }})"/>
	                    	<x-action.delete wire:click="destroy({{ $address->id }})"/>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
		    	</tbody>
			    <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
			</table>
		</div>
		{{ $list_address->links() }}
	</div>
</div>