<div>
	<div class="card">
        <div class="card-body">
            <h4 class="card-title my-0 py-0"><i class="text-20 i-Map2"></i> Domicilios de {{$customer->nombre}} (#{{$customer->id}})</h4>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	@include("{$includes}.form")
	    </div>
		<div class="col-md-9 mb-3">
			<!--include("{$includes}.filters")-->
			@include("{$includes}.list")
		</div>
	</div>
	@if($data_model && $show_modal)	
		@include("{$includes}.modal")
	@endif
</div>
