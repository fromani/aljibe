<div class="card mt-2">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de Configuraciones </h4>
	    <div class="table-responsive">
			<table class="table w-full">
			    <thead>
			        <tr class="bg-gray-600 text-white">
			            <th class="px-4 py-2" scope="col">#</th>
			            <th class="px-4 py-2" scope="col">Producto</th>
			            <th class="px-4 py-2" scope="col">Cantidad</th>
			            <th class="px-4 py-2" scope="col">Frecuencia</th>
			            <th class="px-4 py-2" scope="col">Fecha Inicio</th>
			            <th class="px-4 py-2" scope="col">Estado</th>
			            <th class="px-4 py-2" scope="col">acciones</th>
			        </tr>
			    </thead>
			    <tbody>
			    	@foreach($list_configs as $config)
	                <tr @if($this->editing($config->id))class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$config->id}}</th>
	                    <td>{{ $config->product ? $config->product->nombre : '' }}</td>
	                    <td>{{ $config->cantidad }}</td>
	                    <td>{{ $config->frecuencia }}</td>
	                    <td>@dmy($config->fecha_inicio)</td>
	                    <td><h5>{!! $config->estado !!}</h5></td>
	                    <td class="d-flex justify-content-center">			                    	
	                    	@if($config->deleted_at)
	                    	<x-action.restore wire:click="restore({{ $config->id }})"/>
	                    	@else
	                    	<x-action.edit wire:click="edit({{ $config->id }})"/>
	                    	<x-action.delete wire:click="destroy({{ $config->id }})"/>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
		    	</tbody>
			    <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
			</table>
		</div>
	</div>
</div>