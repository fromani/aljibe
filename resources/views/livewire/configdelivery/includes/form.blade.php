<x-frontend.card class="mt-2">
    <x-slot name="header">
        <h4 class="card-title my-2"> {{ $form['title'] }} </h4>
    </x-slot>

    <x-slot name="body">       
        <x-forms.post wire:submit.prevent="{{ $form['action'] }}">

            <x-input.select name="product_id" :label="'Producto'" wire:model.defer="product_id">
                <x-slot name="options">
                    <option value="">Seleccione uno</option>
                    @foreach($products as $product)
                        <option value="{{ $product->id }}">{{ $product->nombre }}</option>
                    @endforeach
                </x-slot>
            </x-input.select>

            <x-input.number name="cantidad" id="cantidad" :label="'Cantidad'" wire:model.defer="cantidad" placeholder="cantidad de productos" required/>

            <x-input.number name="frecuencia" id="frecuencia" :label="'Frecuencia'" wire:model.defer="frecuencia" placeholder="frecuencia en días" required/><small>Este campo determina cada cuántos días se debe repartir el producto</small>

            <x-input.date name="fecha_inicio" id="fecha_inicio" :label="'Fecha de inicio'" wire:model.defer="fecha_inicio" required/>

            @if($this->editing($keyid))
            <br>
            <x-input.switch name="suspended" id="suspended" label="Suspendido" wire:model="suspended" switchclass="danger"/>
            <br>
            @endif
            <x-button.save label="Guardar" />

	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-2" type="button" wire:click="default">Cancelar</button>
	    	@endif

        </x-forms.post>
    </x-slot>
</x-frontend.card>