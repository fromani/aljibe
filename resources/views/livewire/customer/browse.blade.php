<div>
	<x-utils.message/>
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	@include("{$includes}.form")
	    </div>
		<div class="col-md-9 mb-3">
			@include("{$includes}.filters")
			@include("{$includes}.list")
		</div>
	</div>
	@if($data_model && $show_modal)	
		@include("{$includes}.modal")
	@endif
</div>
