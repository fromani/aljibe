<div 
  x-data
  id="modalLive" 
  class="modal fade show"
  tabindex="-1" 
  role="dialog" 
  aria-labelledby="modalLiveLabel" 
  style="display: block; padding-right: 17px;background-color: #3f42448a;"      
>    
  <div 
    class="modal-dialog modal-lg"
    style="max-width: 90%" 
    role="document"
    id="modalContent"
    x-on:click.away="$wire.closemodal()"
  >
    <div class="modal-content">          
      <div class="modal-header">
        <h5 class="modal-title" id="modalLiveLabel">DOMICILIOS DE {{ $data_model->nombre }} (#{{ $data_model->id }})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="$emitUp('modalClose')">
          <span aria-hidden="true">x</span>
        </button>
      </div>
      <div class="modal-body">            
        @livewire('address.address-component', ['customer' => $data_model], key('addresses_'.$data_model->id))
      </div>   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="$emitUp('modalClose')">Cerrar</button>
      </div>       
    </div>
  </div>      
</div> 