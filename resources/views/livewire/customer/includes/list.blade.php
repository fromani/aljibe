<div class="card mt-2">
    <div class="card-body">
        <h4 class="card-title mb-3"> Listado de Clientes </h4>
	    <div class="table-responsive">
			<table class="table w-full">
			    <thead>
			        <tr class="bg-gray-600 text-white">
			            <th class="px-4 py-2" scope="col">#</th>
			            <th class="px-4 py-2" scope="col"><x-table.sort :label="'Nombre'" :column="'nombre'" sorted="{{$sorted_column}}" direction="{{$sort_direction['dir']}}"/></th>
			            <th class="px-4 py-2" scope="col">Identificador</th>
			            <th class="px-4 py-2" scope="col">Teléfono</th>
			            <th class="px-4 py-2" scope="col">Domicilios</th>
			            <th class="px-4 py-2" scope="col">Estado</th>
			            <th class="px-4 py-2" scope="col">acciones</th>
			        </tr>
			    </thead>
			    <tbody>
			    	@foreach($list_customers as $cust)
	                <tr @if($this->editing($cust->id))class="font-weight-bold bg-light text-dark" @endif>
	                    <th scope="row">{{$cust->id}}</th>
	                    <td>{{$cust->nombre}}</td>
	                    <td>{{$cust->cuit}}</td>
	                    <td><x-utils.link target="_blank" class="btn btn-link" icon="fab fa-whatsapp" href="https://api.whatsapp.com/send?phone={{preg_replace('/(\+54 9 )?[\(\)\-\s]+/', null, $cust->telefono)}}" text="{{$cust->telefono}}"/></td>
	                    <td class="d-flex justify-content-center">
	                    @if($cust->addresses->count()>0)
	                    	<x-utils.link class="btn btn-warning" icon="fas fa-list" wire:click="openAddresses({{ $cust->id }})">
	                    		<span class="badge badge-light">{{ $cust->addresses->count() }}</span>
	                    	</x-utils.link>
	                    @else
	                    	<x-utils.link href="javascript:void(0);" class="btn btn-sm btn-primary" icon="fas fa-plus" wire:click="openAddresses({{ $cust->id }})"/>
	                    @endif
	                    </td>
	                    <td><h5>{!! $cust->estado !!}</h5></td>
	                    <td class="d-flex justify-content-center">			                    	
	                    	@if($cust->deleted_at)
	                    	<x-action.restore wire:click="restore({{ $cust->id }})"/>
	                    	@else
	                    	<x-action.edit wire:click="edit({{ $cust->id }})"/>
	                    	<x-action.delete wire:click="destroy({{ $cust->id }})"/>
	                    	@endif			                    	
	                    </td>
	                </tr>
	                @endforeach
			    </tbody>
			    <tfoot>
	            	<tr>
	            		<td colspan="8">
	            			@if(session()->get('flash_info'))
							    <x-utils.alert type="info" class="header-message">
							        {{ session()->get('flash_info') }}
							    </x-utils.alert>
							@endif
	            		</td>
	            	</tr>
	            </tfoot>
			</table>
			{{ $list_customers->links() }}
		</div>
	</div>
</div>