<div x-data="{
    show: @entangle('show_modal'), 
    open() { this.show = 1 }, 
    close() { this.show = 0;   }, 
    isOpen() { return this.show === 1 }
  }" 
  x-on:open-modal.window="open()"
>
  <template x-if="isOpen()">
    <div 
      id="modalLive" 
      class="modal fade show"
      tabindex="-1" 
      role="dialog" 
      aria-labelledby="modalLiveLabel" 
      style="display: block; padding-right: 17px;background-color: #3f42448a;"      
    >    
      <div 
        class="modal-dialog modal-lg"
        style="max-width: 90%" 
        role="document"
        @click.away="close()"
        x-show="isOpen()"
      >
        <div class="modal-content">          
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLiveLabel">DOMICILIOS DE {{ $data_model->nombre }} (#{{ $data_model->id }})</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" x-on:click="close()">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">            
            @livewire('address.address-component', ['customer' => $data_model], key('addresses_'.$data_model->id))
          </div>          
        </div>
      </div>      
    </div>    
  </template>    
</div>

  