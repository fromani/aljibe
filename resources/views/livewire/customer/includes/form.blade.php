<x-frontend.card>
    <x-slot name="header">
        <h4 class="card-title my-2"> {{ $form['title'] }} </h4>
    </x-slot>

    <x-slot name="body">

        <x-forms.post wire:submit.prevent="{{ $form['action'] }}">

            <x-input.text name="nombre" id="nombre" :label="'Nombre de cliente'" wire:model.defer="nombre" required/>

            <x-input.number name="cuit" id="cuit" :label="'Indentificador'" wire:model.defer="cuit" placeholder="DNI / CUIL / CUIT"/>

            <x-input.phone name="telefono" id="telefono" :label="'Teléfono'" wire:model.defer="telefono"/>

            <x-button.save label="Guardar"/>

	    	@if($form['view']=='edit')
	    	<button class="btn btn-outline-secondary m-2" type="button" wire:click="default">Cancelar</button>
	    	@endif
        </x-forms.post>

    </x-slot>
</x-frontend.card>