<x-frontend.card>
    <x-slot name="header">
        <i class="fas fa-search"></i> Buscador
    </x-slot>

    <x-slot name="body">

        <x-forms.get class="form-inline" wire:submit.prevent="render">

            <x-input.switch name="filter_active" id="filter_active" label="Activos" wire:model="filter_active" switchclass="success" checked="checked"/>

            <x-input.switch name="filter_removed" id="filter_removed" label="Eliminados" wire:model="filter_removed" switchclass="danger"/>

        	<x-input.search name="search" id="search" placeholder="Nombre de cliente" wire:model.debounce.800ms="search"/>

        </x-forms.get>

    </x-slot>
</x-frontend.card>