<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
        <ul class="navigation-left">
            @if ($logged_in_user->isUser())
            <x-frontend.menuitem
                :href="route('frontend.user.dashboard')"
                :active="activeClass(Route::is('frontend.user.dashboard'))"
                :text="__('Dashboard')"
                :icon="'nav-icon i-Bar-Chart'"
                class="nav-item"/>
            <x-frontend.menuitem
                :href="route('frontend.user.products')"
                :active="activeClass(Route::is('frontend.user.products'))"
                :text="__('Products')"
                :icon="'nav-icon i-Shop-4'"
                :permission="'product.access'"
                class="nav-item"/>
            <x-frontend.menuitem
                :href="route('frontend.user.serviceplans')"
                :active="activeClass(Route::is('frontend.user.serviceplans'))"
                :text="__('Services')"
                :icon="'nav-icon i-Support'"
                :permission="'serviceplan.access'"
                class="nav-item"/>
            <x-frontend.menuitem
                :href="route('frontend.user.distributors')"
                :active="activeClass(Route::is('frontend.user.distributors'))"
                :text="__('Distributors')"
                :icon="'nav-icon i-Cool-Guy'"
                :permission="'distributor.access'"
                class="nav-item"/>
            <x-frontend.menuitem
                :href="route('frontend.user.customers')"
                :active="activeClass(Route::is('frontend.user.customers'))"
                :text="__('Customers')"
                :icon="'nav-icon i-Conference'"
                :permission="'customer.access'"
                class="nav-item"/>
            @endif 
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>