<div class="main-header">
    <div class="logo">
        <img src="{{ asset('img/brand/swai_signet-color.png') }}" alt="">
    </div>
    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="d-flex align-items-center">
    </div>
    <div style="margin: auto"></div>
    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->
        @php
        $items = [[ 'href' => route('frontend.user.products'), 
                    'text' => 'Productos', 
                    'icon' => 'i-Shop-4', 
                    'permission' => 'product.access'
                 ], [
                    'href' => route('frontend.user.serviceplans'), 
                    'text' => __('Services'), 
                    'icon' => 'i-Support', 
                    'permission' => 'serviceplan.access'
                 ]];
        @endphp
        @include('frontend.includes.partials.gridmenu', ['items' => $items])
        <!-- Notificaiton -->
        @include('frontend.includes.partials.notification')
        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
                <img src="{{ asset('img/yourlogo-icon.png') }}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i> {{ $logged_in_user->name }}
                    </div>
                    <x-utils.link
                                :href="route('frontend.user.account')"
                                :active="activeClass(Route::is('frontend.user.account'))"
                                :text="__('My Account')"
                                class="dropdown-item" />
                    <x-utils.link
                        :text="__('Logout')"
                        class="dropdown-item"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <x-slot name="text">
                            @lang('Logout')
                            <x-forms.post :action="route('frontend.auth.logout')" id="logout-form" class="d-none" />
                        </x-slot>
                    </x-utils.link>
                </div>
            </div>
        </div>
    </div>
</div>