@if (Breadcrumbs::has() && !Route::is('frontend.index'))
    @php 
        $current_breadcrumbs = Breadcrumbs::current();
    @endphp
    <div class="main-content">
        <div class="d-flex justify-content-between">
            <div class="breadcrumb">
                <h1>{{ $current_breadcrumbs->last()->title() }}</h1>
                <ul>
                @foreach ($current_breadcrumbs as $crumb)
                    @if ($crumb->url() && !$loop->last)
                        <li><x-utils.link :href="$crumb->url()" :text="$crumb->title()" /></li>
                    @else
                        <li>{{ $crumb->title() }}</li>
                    @endif
                @endforeach
                </ul>
            </div>
            @yield('right-nav-items')
        </div>
        <div class="separator-breadcrumb border-top"></div><!-- end of main-content -->
    </div><!-- Footer Start -->
@endif
