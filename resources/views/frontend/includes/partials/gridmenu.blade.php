<div class="dropdown">
    <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownSubMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
    <div class="dropdown-menu" aria-labelledby="dropdownSubMenuButton">
        <div class="menu-icon-grid">
    	@foreach($items as $item)  
        	@php
        	$href = $item['href'];
        	$icon = $item['icon'];
        	$text = $item['text'];
        	$permission = $item['permission'];
        	@endphp      	
        	<a href="{{ $href }}"><i class="{{ $icon }}"></i> {{ $text }}</a>
        @endforeach            
        </div>
    </div>
</div>