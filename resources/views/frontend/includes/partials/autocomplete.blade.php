<div x-data="{
            highlightIndex: 0,
            items: @entangle('records'),
            sizeDropdown() {
                return this.items.length;
            },
            incrementHighlight() {
                if(this.highlightIndex === this.sizeDropdown()) {
                    this.highlightIndex = 0;
                    return;
                }
                this.highlightIndex++;
            },
            decrementHighlight() {
                if(this.highlightIndex === 0) {
                    this.highlightIndex = 0;
                    return;
                }
                this.highlightIndex--;
            }
        }"
    class="form-group has-search mb-1"
>
    <label for="">{{ $label }}</label>
    <div wire:loading.remove><span class="fa fa-search form-control-feedback"></span></div>
    <div wire:loading><i class="fas fa-spinner fa-pulse form-control-feedback"></i></div>   
    <input
        x-on:keydown.arrow-up="decrementHighlight()"
        x-on:keydown.arrow-down="incrementHighlight()"
        x-on:keydown.enter.prevent="$wire.selectOption(highlightIndex)" 
        type="text"
        class="form-control"
        placeholder="Buscar ..."
        wire:model="query"
        wire:keydown.escape="default"
    />
    @if($option_selected==-1 && Str::length($query)>=2)
        <div class="absolute z-10 list-group rounded-t-none shadow-lg">
            @if(!empty($records))
            <template x-for="(item, index) in items">
                <a
                    href="#"
                    x-on:click="$wire.selectOption(index)"
                    class="list-group-item list-group-item-action"
                    x-bind:class="{ 'active': index==highlightIndex }"
                    x-text="item['text']"
                    ></a>
            </template>
            @else
                <div class="list-group-item list-group-item-action disabled">No hay resultados!</div>
            @endif
        </div>
    @endif
    @error($name)
        <small class="text-danger"> {{ $message }}</small>
    @enderror
</div>