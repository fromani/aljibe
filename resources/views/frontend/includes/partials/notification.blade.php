<div class="dropdown">
    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="badge badge-primary">1</span>
        <i class="i-Bell text-muted header-icon"></i>
    </div>
    <!-- Notification dropdown -->
    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
        <div class="dropdown-item d-flex">
            <div class="notification-icon">
                <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
            </div>
            <div class="notification-details flex-grow-1">
                <p class="m-0 d-flex align-items-center">
                    <span>Mensaje</span>
                    <span class="badge badge-pill badge-primary ml-1 mr-1">nuevo</span>
                    <span class="flex-grow-1"></span>
                    <span class="text-small text-muted ml-auto">hace 10 días</span>
                </p>
                <p class="text-small text-muted m-0">Federico: el sistema se encuentra en eatpa de desarrollo</p>
            </div>
        </div>
    </div>
</div>