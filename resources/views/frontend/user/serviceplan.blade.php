@extends('frontend.layouts.app')

@section('title', __('Service Plans'))

@section('content')
<livewire:serviceplan.browse />
@endsection

@push('after-scripts')
<script type="text/javascript" charset="utf-8" async defer>
 	$('document').ready(function(){
 		let component = window.livewire.components.getComponentsByName("serviceplan.browse")[0];
 		document.getElementById('nombre').focus();
	});
</script>
@endpush