@extends('frontend.layouts.app')

@section('title', __('Distributors'))

@section('content')
<livewire:distributor.browse />
@endsection

@push('after-scripts')
<script type="text/javascript" charset="utf-8" async defer>
 	$('document').ready(function(){
 		document.getElementById('nombre').focus();
	});
</script>
@endpush