@extends('frontend.layouts.app')

@section('title', __('Products'))

@section('content')
	<livewire:product.browse />
@endsection

@push('after-scripts')
<script src="{{ asset('js/plugins/vanilla-picker.min.js') }}"></script>
<link href="{{ asset('css/plugins/vanilla-picker.css') }}" rel="stylesheet">
<script type="text/javascript" charset="utf-8" async defer>
 	$('document').ready(function(){
 		document.getElementById('category_id').focus();
	});
</script>
@endpush