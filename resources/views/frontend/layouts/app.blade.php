<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="description" content="@yield('meta_description', appName())">
    <meta name="author" content="@yield('meta_author', 'Romani Federico')">
    @yield('meta')

    @stack('before-styles')
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')
</head>
<body class="text-left">
	@include('includes.partials.read-only')
	@include('includes.partials.logged-in-as')
	@include('includes.partials.announcements')

    <div class="app-admin-wrap layout-sidebar-large">
        @include('frontend.includes.mainheader')
        @include('includes.partials.messages')
        @include('frontend.includes.sidebarleft')
        <!-- =============== Left side End ================-->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <!-- ============ Body content start ============= -->
            @if (config('boilerplate.frontend_breadcrumbs'))
                @include('frontend.includes.partials.breadcrumbs')
            @endif           
            <div class="flex-grow-1">
                @yield('content')
            </div>
            <!-- Footer Start -->
            <div class="app-footer">
                <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
                    <div class="d-flex align-items-center">
                        <img class="logo" src="{{ asset('img/brand/swai_signet-color.png') }}" alt="">
                        <div>
                            <p class="m-0">&copy; {{ date("Y") }} SWAI Soluciones Web</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fotter end -->
        </div>
    </div>
    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>
    <livewire:scripts />
    @stack('after-scripts')
</body>
</html>