@props(['active' => '', 'text' => '', 'hide' => false, 'icon' => false, 'permission' => false])

@if ($permission)
    @if ($logged_in_user->can($permission))
        @if (!$hide)
        	<li {{ $attributes->merge(['class' => $active]) }}>
	            <a class="nav-item-hold" {{ $attributes->merge(['href' => '#']) }}>@if ($icon)<i class="{{ $icon }}"></i> @endif<span class="nav-text">{{ strlen($text) ? $text : $slot }}</span></a>
	            <div class="triangle"></div>
            </li>
        @endif
    @endif
@else
    @if (!$hide)
        <li {{ $attributes->merge(['class' => $active]) }}>
            <a class="nav-item-hold" {{ $attributes->merge(['href' => '#']) }}>@if ($icon)<i class="{{ $icon }}"></i> @endif<span class="nav-text">{{ strlen($text) ? $text : $slot }}</span></a>
            <div class="triangle"></div>
        </li>
    @endif
@endif