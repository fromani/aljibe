<input 
	x-data="{telefono:''}"
	x-init="
		maskOptions = {
			mask: '(000)-000-0000'
		};
		let mask = IMask($refs.phoneinput, maskOptions);
	"
	x-ref="phoneinput"
	x-model="telefono"
	{{ $attributes }} 
	class="form-control" 
/>
<script src="{{ asset('js/plugins/imask.min.js') }}" defer></script>
