@props(['products' => []])
<div x-data="objProductList" @notify-edit-service.window="open=false" >

    <span @click="open=!open">{{ $trigger }}</span>

    <div x-show="open"><!-- @click.away="open = false" -->
        {{ $slot }}
    </div>
</div>
@push('after-scripts')
<script>
var default_prod = "{{ ($products->first()) ? $products->first()->id:'' }}";
var objProductList = {
		open: false,
        productId: default_prod,
        optionsProducts: {!! 
	        $products->toJson() 
        !!},
        cantidad: 1,
        addProduct(dispatch) {
        	dispatch('notify-add-product', {
                id: this.productId,
                nombre: this.getProductoNombre(this.productId),
                pivot: {cantidad: this.cantidad}
            });
            this.productId = default_prod;
            this.cantidad = 1;
        },
        getProductoNombre(key) {
            let obj = this.optionsProducts.find( ({ id }) => parseInt(id) === parseInt(key));
            return obj.nombre;
        },
        productCount() {
            return this.products.length
        },
        isLastProduct(index) {
            return this.products.length - 1 === index
        }
    };
</script>
@endpush