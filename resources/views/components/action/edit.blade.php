@props(['label' => '', 'permission' => false])

@if ($permission)
    @if ($logged_in_user->can($permission))
		<button class="btn btn-success btn-sm mr-2" {{ $attributes }}><i class="nav-icon i-Pen-2 font-weight-bold"></i> {{ $label }}</button>
    @endif
@else
    <button class="btn btn-success btn-sm mr-2" {{ $attributes }}><i class="nav-icon i-Pen-2 font-weight-bold"></i> {{ $label }}</button>
@endif