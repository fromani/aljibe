@props(['label' => '', 'permission' => false])

@if ($permission)
    @if ($logged_in_user->can($permission))
		<button class="btn btn-warning btn-sm mr-2" {{ $attributes }}><i class="fas fa-trash-restore"></i> {{ $label }}</button>
    @endif
@else
    <button class="btn btn-warning mr-2" {{ $attributes }}><i class="fas fa-trash-restore"></i> {{ $label }}</button>
@endif