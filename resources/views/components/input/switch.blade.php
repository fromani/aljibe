@props(['permission' => false, 'label' => false, 'message' => '', 'switchclass' => 'default'])

@if ($permission)
  @if ($logged_in_user->can($permission))
  	<div class="custom-control mx-2">
        <label class="switch pr-3 switch-{{$switchclass}} mr-2"><span>{{ $label }}</span>
          <input type="checkbox" {{ $attributes }}><span class="slider"></span>
          @error($attributes['name'])
            <small class="text-danger"> {{ $message }}</small>
          @enderror
        </label>
        {{ $slot }}        
    </div>
  @endif
@else
    <div class="custom-control custom-switch mx-2">
  		<label class="switch pr-3 switch-{{$switchclass}} mr-2"><span>{{ $label }}</span>
        <input type="checkbox" {{ $attributes }}><span class="slider"></span>
        @error($attributes['name'])
          <small class="text-danger"> {{ $message }}</small>
        @enderror
      </label>
      {{ $slot }}      
	</div>
@endif