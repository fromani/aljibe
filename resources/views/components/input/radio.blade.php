@props(['icon' => false, 'permission' => false, 'label' => false, 'message' => ''])

@if ($permission)
    @if ($logged_in_user->can($permission))
	<div class="form-check">
  		<input type="radio" class="form-check-input @error($attributes['name']) is-invalid @enderror" {{ $attributes }}>
  		<label class="form-check-label" for="{{ $attributes['id'] }}">{{ $label }} </label>
	</div>
    @endif
@else
	<div class="form-check">
  		<input type="radio" class="form-check-input @error($attributes['name']) is-invalid @enderror" {{ $attributes }}>
  		<label class="form-check-label" for="{{ $attributes['id'] }}">{{ $label }} </label>
	</div>
@endif