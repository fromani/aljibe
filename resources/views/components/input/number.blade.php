@props(['icon' => false, 'permission' => false, 'label' => false, 'message' => ''])
@if ($permission)
    @if ($logged_in_user->can($permission))
	<div class="form-group mb-1">
		@if ($label)
		<label for="">{{ $label }}</label>
		@elseif (!$label && $icon)
		<div class="input-group-prepend">
		    <span class="input-group-text">
		    	<i class="{{ $icon }}"></i>
		    </span>
	  	</div>
		@else
	  		{{ $slot }}
		@endif
	    <input type="number" {{ $attributes->merge(['class' => 'form-control']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'form-control is-invalid']) }} @enderror value="">
	    {{ $slot }}
		@error($attributes['name'])
			<small class="text-danger"> {{ $message }}</small>
		@enderror
	</div>
    @endif
@else
	<div class="form-group mb-1">
		@if ($label)
		<label for="">{{ $label }}</label>
		@elseif (!$label && $icon)
		<div class="input-group-prepend">
		    <span class="input-group-text">
		    	<i class="{{ $icon }}"></i>
		    </span>
	  	</div>
		@else
	  		{{ $slot }}
		@endif
	    <input type="number" {{ $attributes->merge(['class' => 'form-control']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'form-control is-invalid']) }} @enderror value="">
	    {{ $slot }}
		@error($attributes['name'])
			<small class="text-danger"> {{ $message }}</small>
		@enderror
	</div>
@endif