@props(['icon' => false, 'permission' => false, 'label' => false, 'message' => ''])

@if ($permission)
    @if ($logged_in_user->can($permission))
	<div class="custom-control custom-switch mx-2">
      <input type="checkbox" {{ $attributes->merge(['class' => 'custom-control-input']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'custom-control-input is-invalid']) }} @enderror>
      @if($label)
        <label class="custom-control-label" for="{{ $attributes['id'] }}">{{ $label }} </label>
      @else
        {{ $slot }}
      @endif
  </div>
    @endif
@else
    <div class="custom-control custom-switch mx-2">
  		<input type="checkbox" {{ $attributes->merge(['class' => 'custom-control-input']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'custom-control-input is-invalid']) }} @enderror>
  		@if($label)
        <label class="custom-control-label" for="{{ $attributes['id'] }}">{{ $label }} </label>
      @else
        {{ $slot }}
      @endif
	</div>
@endif