@props(['permission' => false, 'message' => ''])

@if ($permission)
    @if ($logged_in_user->can($permission))
	<div class="form-group has-search mb-1 mx-2">
		<span class="fas fa-search form-control-feedback"></span>
	    <input type="text" {{ $attributes->merge(['class' => 'form-control']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'form-control is-invalid']) }} @enderror value="">
	    <small class="text-info m-2" wire:loading wire:target="search">
        	buscando resultados...
    	</small>
	    @error($attributes['name'])
			<small class="text-danger"> {{ $message }}</small>
		@enderror
	</div>
    @endif
@else
	<div class="form-group has-search mb-1 mx-2">
		<span class="fas fa-search form-control-feedback"></span>
	    <input type="text" {{ $attributes->merge(['class' => 'form-control']) }} @error($attributes['name']) {{ $attributes->merge(['class' => 'form-control is-invalid']) }} @enderror value="">
	    <small class="text-info m-2" wire:loading wire:target="search">
        	buscando resultados...
    	</small>
	    @error($attributes['name'])
			<small class="text-danger"> {{ $message }}</small>
		@enderror
	</div>
@endif