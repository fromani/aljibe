@props(['permission' => false, 'label' => false, 'message' => ''])

@if ($permission)
    @if ($logged_in_user->can($permission))
	<div class="form-group mb-1">
		@if ($label)
		<label for="">{{ $label }}</label>
		@else
	  		{{ $slot }}
		@endif
		<div class="input-group mb-3">
			<div class="input-group-prepend">
			    <span class="input-group-text" id="phone-prepend">
			    	<i class="fas fa-phone"></i>
			    </span>
		  	</div>		
		    @php $attrs = $attributes->merge(['class' => 'form-control']) @endphp
			@error($attributes['name'])
				@php $attrs = $attributes->merge(['class' => 'form-control is-invalid']) @endphp
			@enderror
		    <input type="text" 
				x-data="{telefono:''}"
				x-init="
					maskOptions = {
						mask: '+{54} 9 (000) 000-0000',
						lazy: false, 
						placeholderChar: '#' 
					};
					let mask = new IMask.InputMask($refs.phoneinput, maskOptions);
				"
				x-ref="phoneinput"
				x-model="telefono"
				{{ $attrs }}
				class="form-control" 
				aria-describedby="phone-prepend"
			/>
		    @error($attributes['name'])
				<small class="text-danger"> {{ $message }}</small>
			@enderror
		</div>
	</div>
    @endif
@else
	<div class="form-group mb-1">
		@if ($label)
		<label for="">{{ $label }}</label>
		@else
	  		{{ $slot }}
		@endif
		<div class="input-group mb-3">
			<div class="input-group-prepend">
			    <span class="input-group-text" id="phone-prepend">
			    	<i class="fas fa-phone"></i>
			    </span>
		  	</div>		
		    @php $attrs = $attributes->merge(['class' => 'form-control']) @endphp
			@error($attributes['name'])
				@php $attrs = $attributes->merge(['class' => 'form-control is-invalid']) @endphp
			@enderror
		    <input type="text" 
				x-data="{telefono:''}"
				x-init="
					maskOptions = {
						mask: '+{54} 9 (000) 000-0000',
						lazy: false, 
						placeholderChar: '#' 
					};
					let mask = new IMask.InputMask($refs.phoneinput, maskOptions);
				"
				x-ref="phoneinput"
				x-model="telefono"
				{{ $attrs }}
				class="form-control" 
				aria-describedby="phone-prepend"
			/>
		    @error($attributes['name'])
				<small class="text-danger"> {{ $message }}</small>
			@enderror
		</div>
	</div>
@endif