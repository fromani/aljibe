@props(['permission' => false, 'label' => false])

@if ($permission)
    @if ($logged_in_user->can($permission))
		<button {{ $attributes->merge(['type' => 'submit']) }} class="btn btn-primary btn-md m-2" {{ $attributes }}><i class="fas fa-save"></i> {{ $label }}</button>
    @endif
@else
	<button {{ $attributes->merge(['type' => 'submit']) }} class="btn btn-primary btn-md m-2" {{ $attributes }}><i class="fas fa-save"></i> {{ $label }}</button>
@endif