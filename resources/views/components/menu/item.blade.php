@props(['active' => '', 'text' => '', 'icon' => false, 'permission' => false, 'level' => 0])

@if ($permission)
    @if ($logged_in_user->can($permission))
	<li class="c-sidebar-nav-item">
	    <a class="c-sidebar-nav-link" {{ $attributes->merge(['href' => '#', 'class' => $active]) }}>@if ($icon)<i class="@if($level==0)c-sidebar-nav-icon @endif{{ $icon }}"></i> @endif{{ strlen($text) ? $text : $slot }}</a>
	</li>
    @endif
@else
    <li class="c-sidebar-nav-item">
	    <a class="c-sidebar-nav-link" {{ $attributes->merge(['href' => '#', 'class' => $active]) }}>@if ($icon)<i class="@if($level==0)c-sidebar-nav-icon @endif{{ $icon }}"></i> @endif{{ strlen($text) ? $text : $slot }}</a>
	</li>
@endif