@props(['text' => '', 'icon' => ''])
<li class="c-sidebar-nav-dropdown">
	<a href="#" class="c-sidebar-nav-dropdown-toggle">
		<i class="c-sidebar-nav-icon {{ $icon }}"></i> <strong>{{ strlen($text) ? $text : '' }} </strong>
	</a>
	<ul class="c-sidebar-nav-dropdown-items">		
		{{ $slot }}
	</ul>
</li>