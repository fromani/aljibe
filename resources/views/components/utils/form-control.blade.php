@props([
	'col'   	 => '',
    'control_key' => '',
    'label' 	 => false,
    'control' 	 => '',
    'message' 	 => '',
    'permission' => false,
])

@if ($permission)
    @if ($logged_in_user->can($permission))
		<div class="col-md-{{ $col }} mb-3">
			@if($label)<label for="{{ $control_key }}">{{ $label }}</label>@endif
			{{ $control }}
			<div class="invalid-feedback">
				El campo {{ $label }} es incorrecto.
			</div>
			@error($control_key)
				<small class="text-danger"> {{ $message }}</small>
			@enderror
			{{ $slot }}
		</div>
    @endif
@else
	<div class="col-md-{{ $col }} mb-3">
		@if($label)<label for="{{ $control_key }}">{{ $label }}</label>@endif
		{{ $control }}
		{{ $slot }}
	</div>
@endif