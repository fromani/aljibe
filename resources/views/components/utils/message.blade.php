@if(session()->get('flash_success'))
    <script type="text/javascript" >
        toastr.remove();
        toastr.clear();
        toastr.success("{{ session()->get('flash_success') }}", 'Info', {timeOut: 5000, progressBar:true})
    </script>
@elseif(session()->get('flash_warning'))	
    <script type="text/javascript" >
        toastr.remove();
        toastr.clear();
        toastr.warning("{{ session()->get('flash_warning') }}", 'Info', {timeOut: 5000, progressBar:true});
    </script>
@elseif(session()->get('flash_error'))
    <script type="text/javascript" >
        toastr.remove();
        toastr.clear();
        toastr.error("{{ session()->get('flash_error') }}", 'Info', {timeOut: 5000, progressBar:true})
    </script>
@elseif(session()->get('flash_info') || session()->get('flash_message'))
    <script type="text/javascript" >
        toastr.remove();
        toastr.clear();
        toastr.info("{{ session()->get('flash_info') }}", 'Info', {timeOut: 5000, progressBar:true})
    </script>
@endif