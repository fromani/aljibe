<div>
	<x-utils.message/>
    <div class="row">
	    <div class="col-md-3 mb-3">
	    	{{ $form }}
	    </div>
		<div class="col-md-9 mb-3">
			{{ $filters }}
			{{ $list }}
		</div>
	</div>
	{{ $slot }}
</div>