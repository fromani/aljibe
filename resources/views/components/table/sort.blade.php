@props([
	'label' => '', 
	'column' => '', 
	'direction' => 'up',
	'sorted' => ''
])
<a href="javascript:void(0)" class="text-white text-decoration-none" wire:click="sortBy('{{ $column }}')">
	{{ $label }} 
	@if($sorted === $column)
	<i class="fas fa-chevron-{{ $direction }} pl-2"></i>
	@endif
</a>