<footer class="c-footer">
    <div>
        <strong>
            @lang('Copyright') &copy; {{ date('Y') }}
            <x-utils.link href="https://swai.com.ar" target="_blank" :text="__(appName())" />
        </strong>

        @lang('All Rights Reserved')
    </div>

    <div class="mfs-auto">
        @lang('Powered by')
        <x-utils.link href="https://swai.com.ar" target="_blank" :text="__(appName())" /> &
        <x-utils.link href="https://swai.com.ar" target="_blank" text="SWAI" />
    </div>
</footer>
