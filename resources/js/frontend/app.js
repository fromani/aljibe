import 'alpinejs'

require('../bootstrap');
require('perfect-scrollbar').default;
require('./scripts/script.js');
require('./scripts/sidebar.large.script.js');

window.Vue = require('vue');

Vue.component('welcome-component', require('./components/WelcomeComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*
const app = new Vue({
    el: '#app',
});
*/